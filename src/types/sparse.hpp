#ifndef __SPARSE_H_
#define __SPARSE_H_
#include <map>
#include <stdexcept>
#include <iostream>
#include <memory>
#include "complex.h"

template<typename T>
struct CSRMatrix
    {
    unsigned int len;
    unsigned int dim;
    unsigned int* col;
    unsigned int* row_s;
    T* val;
    };


class key
    {
    public:
        unsigned int row;
        unsigned int col;
        key():row(0),col(0){};
        key(unsigned int r,unsigned int c):row(r),col(c){};
        ~key(){};
    };

bool operator <(const key &,const key &);

template<typename T>
class sparse_matrix;

template<typename T>
sparse_matrix<T> trans(const sparse_matrix<T> &a);

template<typename T>
sparse_matrix<T> conj(const sparse_matrix<T> &a);

template<typename T>
class sparse_matrix
    {
    private:
        friend sparse_matrix<T> trans<T>(const sparse_matrix<T> &a);
        friend sparse_matrix<T> conj<T>(const sparse_matrix<T> &a);
    public:

    protected:    
        std::map<key,T> matrix_data;
        unsigned int dim;
    public:
        using iterator=typename std::map<key,T>::iterator;
        
        iterator begin()
            {
            return matrix_data.begin();
            }
        
        iterator end()
            {
            return matrix_data.end();
            }
        
        sparse_matrix(const unsigned int &in):dim(in){};
        sparse_matrix(const sparse_matrix &in)
            {
            dim=in.dim;
            matrix_data=in.matrix_data;
            }
        ~sparse_matrix(){};
        void SetElement(const unsigned int& col, const unsigned int& row, const T& el)
            {
            if(row>=dim ||col>=dim)
                throw std::invalid_argument("out of range"); 
            if(el!=T(0.0))
                matrix_data[key(row,col)]=el;
            }
        T&& GetElement(const unsigned int& col, const unsigned int& row)
            {
            if(row>=dim ||col>=dim)
                throw std::invalid_argument("out of range"); 
            if(matrix_data.find(key(row,col))==matrix_data.end())
                return T(0.0);
            else
                return T(matrix_data[key(row,col)]);
            }
        T& operator [](const key &in)
            {
            if(in.row>=dim ||in.col>=dim)
                throw std::invalid_argument("out of range"); 
            return matrix_data[in];
            }
        sparse_matrix & operator =(const sparse_matrix &in)
            {
            matrix_data.clear();
            dim=in.dim;
            matrix_data=in.matrix_data;
            return *this;
            }
        bool operator ==(const sparse_matrix &a) const
            {
            if(a.matrix_data.size()!=matrix_data.size() || a.dim!=dim)
                return false;
            for(auto it=matrix_data.begin();it!=matrix_data.end();++it)
                {
                auto it1=a.matrix_data.find(it->first);
                if(it1==a.matrix_data.end())
                    return false;
                if(it1->second!=it->second)
                    return false;
                }
            return true;
            }
        bool operator !=(const sparse_matrix &a) const
            {
            return !(a==*this);
            }  
        sparse_matrix operator +() const
            {
            return *this;
            }
        sparse_matrix operator -() const
            {
            sparse_matrix per(*this);
            for(auto it=per.matrix_data.begin();it!=per.matrix_data.end();++it)
                it->second=-it->second;
            return per;
            }
        sparse_matrix operator +(const sparse_matrix &a) const
            {
            if(dim!=a.dim)
                throw std::invalid_argument("matrix dimentions don't match"); 
            sparse_matrix per(*this);
            for(auto it=a.matrix_data.begin();it!=a.matrix_data.end();++it)
                per[it->first]+=it->second;
            return per;
            }
        sparse_matrix operator -(const sparse_matrix &a) const
            {
            if(dim!=a.dim)
                throw std::invalid_argument("matrix dimentions don't match"); 
            sparse_matrix per(*this);
            for(auto it=a.matrix_data.begin();it!=a.matrix_data.end();++it)
                per[it->first]-=it->second;
            return per;
            }
        sparse_matrix operator *(const sparse_matrix &a) const
            {
            if(dim!=a.dim)
                throw std::invalid_argument("matrix dimentions don't match"); 
            sparse_matrix per(dim);
            for(auto it=matrix_data.begin();it!=matrix_data.end();++it)
                {
                for(unsigned int col=0;col<dim;col++)
                    {
                    if(a.matrix_data.find(key(it->first.col,col))!=a.matrix_data.end())
                        {
                        per[key(it->first.row,col)]=it->second*a.matrix_data.find(key(it->first.col,col))->second;
                        }
                    }
                }
            return per;
            }
        void operator +=(const sparse_matrix &a)
            {
            if(dim!=a.dim)
                throw std::invalid_argument("matrix dimentions don't match"); 
            for(auto it=a.matrix_data.begin();it!=a.matrix_data.end();++it)
                matrix_data[it->first]+=it->second;
            }
        void operator -=(const sparse_matrix &a)
            {
            if(dim!=a.dim)
                throw std::invalid_argument("matrix dimentions don't match"); 
            for(auto it=a.matrix_data.begin();it!=a.matrix_data.end();++it)
                matrix_data[it->first]-=it->second;
            }
        void operator *=(const sparse_matrix &a)
            {
            if(dim!=a.dim)
                throw std::invalid_argument("matrix dimentions don't match"); 
            sparse_matrix per=(*this)*a;
            matrix_data.clear();
            matrix_data=per.matrix_data;
            }
        sparse_matrix operator +(const T&a) const
            {
            sparse_matrix per(*this);
            for(unsigned int j=0;j<dim;j++)
                per[key(j,j)]+=a;
            return per;
            }
        sparse_matrix operator -(const T&a) const
            {
            sparse_matrix per(*this);
            for(unsigned int j=0;j<dim;j++)
                per[key(j,j)]-=a;
            return per;
            }
        sparse_matrix operator *(const T&a) const
            {
            sparse_matrix per(*this);
            for(auto it=per.matrix_data.begin();it!=per.matrix_data.end();++it)
                it->second*=a;
            return per;
            }
        sparse_matrix operator /(const T&a) const
            {
            sparse_matrix per(*this);
            for(auto it=per.matrix_data.begin();it!=per.matrix_data.end();++it)
                it->second/=a;
            return per;
            }
        void operator +=(const T&a)
            {
            for(unsigned int j=0;j<dim;j++)
                matrix_data[key(j,j)]+=a;
            }
        void operator -=(const T&a)
            {
            for(unsigned int j=0;j<dim;j++)
                matrix_data[key(j,j)]-=a;
            }
        void operator *=(const T&a)
            {
            for(auto it=matrix_data.begin();it!=matrix_data.end();++it)
                it->second*=a;
            }
        void operator /=(const T&a)
            {
            for(auto it=matrix_data.begin();it!=matrix_data.end();++it)
                it->second/=a;
            }
        sparse_matrix mul_t(const sparse_matrix &a)
            {
            sparse_matrix per(dim*a.dim);
            for(auto it=a.matrix_data.begin();it!=a.matrix_data.end();++it)
                {
                for(auto it1=matrix_data.begin();it1!=matrix_data.end();++it1)
                    {
                    per[key(dim*it->first.row+it1->first.row,dim*it->first.col+it1->first.col)]=it->second*it1->second;
                    }
                }
            return per;
            }
        unsigned int dimension(void) const
            {
            return dim;
            }
        unsigned int nonzero_elements(void) const
            {
            return matrix_data.size();
            }
        
        CSRMatrix<T> ToCSR(void)
            {
            CSRMatrix<T> per;
            unsigned int row_start;
            unsigned int peri;
            per.len=matrix_data.size();
            per.dim=dim;
            per.col=new unsigned int[per.len];
            per.row_s=new unsigned int[dim+1];
            per.val=new T[per.len];
            peri=0;
            row_start=0;
            //per.row_s
            for(auto it=matrix_data.begin();it!=matrix_data.end();++it)
                {
                while(peri<=it->first.row)
                    {
                    per.row_s[peri]=row_start;
                    peri++;
                    }
                per.col[row_start]=it->first.col;
                per.val[row_start]=it->second;
                row_start++;
                }
            while(peri<=dim)
                {
                per.row_s[peri]=row_start;
                peri++;
                }
            return per;
            }
        void out(void)
            {
            /*for(auto it=matrix_data.begin();it!=matrix_data.end();++it)
                {
                std::cout<<it->first.row<<";"<<it->first.col<<":";
                complex<double> per=it->second;
                if(fabs(per.__re)>1e-10)
                    {
                    std::cout<<per.__re;
                    if(fabs(per.__im)>1e-10)
                        std::cout<<"+"<<per.__im<<"i";
                    std::cout<<"\n";
                    }
                else if(fabs(per.__im)>1e-10)
                    {
                    std::cout<<per.__im<<"i\n";
                    }
                else
                    {
                    std::cout<<"0\n";
                    }
                }*/
            for(unsigned int r=0;r<dim;r++)
                {
                for(unsigned int c=0;c<dim;c++)
                    {
                    if(matrix_data.find(key(r,c))!=matrix_data.end())
                        {
                        complex<double> per=matrix_data[key(r,c)];
                        if(fabs(per.__re)>1e-10)
                            {
                            std::cout<<per.__re;
                            if(fabs(per.__im)>1e-10)
                                std::cout<<"+"<<per.__im<<"i";
                            std::cout<<" ";
                            }
                        else if(fabs(per.__im)>1e-10)
                            {
                            std::cout<<per.__im<<"i ";
                            }
                        else
                            {
                            std::cout<<"0! ";
                            }
                        }
                    else
                        {
                        std::cout<<"0 ";
                        }
                    }
                std::cout<<std::endl;
                }
            }
    };

template<typename T>
sparse_matrix<T> trans(const sparse_matrix<T> &a)
    {
    sparse_matrix<T> per(a.dimension());
    for(auto it=a.matrix_data.begin();it!=a.matrix_data.end();++it)
        {
        per[key(it->first.col,it->first.row)]=it->second;
        }
    return per;
    }

template<typename T>
sparse_matrix<T> conj(const sparse_matrix<T> &a)
    {
    sparse_matrix<T> per(a.dimension());
    for(auto it=a.matrix_data.begin();it!=a.matrix_data.end();++it)
        {
        per[key(it->first.col,it->first.row)]=conj(it->second);
        }
    return per;
    }

template<typename T>
sparse_matrix<T> operator +(const T& a,const sparse_matrix<T> &b)
    {
    sparse_matrix<T> per(b);
    for(unsigned int j=0;j<per.dimension();j++)
        per[sparse_matrix<T>::key(j,j)]+=a;
    return per;
    }
template<typename T>
sparse_matrix<T>  operator -(const T& a,const sparse_matrix<T> &b)
    {
    sparse_matrix<T> per(b.dimension());
    per=-b;
    for(unsigned int j=0;j<per.dimension();j++)
        per[sparse_matrix<T>::key(j,j)]+=a;
    return per;
    }
template<typename T>
sparse_matrix<T> operator *(const T& a,const sparse_matrix<T> &b)
    {
    sparse_matrix<T> per(b);
    per*=a;
    return per;
    }

#endif
