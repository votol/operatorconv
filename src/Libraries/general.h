#ifndef __general_h_
#define __general_h_
#include <sys/stat.h>
#include <fstream>
#include <stdexcept>

void WriteNumberOfMatricesToFile(const YAML::Node& config, const unsigned int & num)
	{
	std::string output_bin_file=config["paths"]["output_directory"].as<std::string>()+
                config["output"]["name"].as<std::string>()+".bin";
	std::string output_yaml_file=config["paths"]["output_directory"].as<std::string>()+
            config["output"]["name"].as<std::string>()+".yml";
	std::ofstream ofs(output_yaml_file,std::ofstream::out);
    ofs.close();
    std::ofstream output_bin_file_stream(output_bin_file,std::ofstream::out | std::ofstream::binary);
	output_bin_file_stream.write((char * )&num, 4);
	output_bin_file_stream.close();
	}

#endif /*__general_h_*/
