#ifndef __complex_function_hpp_
#define __complex_function_hpp_
#include "real_function.hpp"
#include "complex.h"

#ifndef i
#define i complex<double>(0.0,1.0)
#endif

namespace Abstr
    {
    class ICFunction
        {
    public:
        ICFunction(){};
        virtual ~ICFunction(){};
        virtual std::shared_ptr<ICFunction> copy(void) const =0;
        virtual std::string to_string(void) const =0;
        };

    class CFunctionContainer
        {
    private:
        std::shared_ptr<ICFunction> content;
    public:
        CFunctionContainer();
        ~CFunctionContainer(){};
        CFunctionContainer(const CFunctionContainer&);
        CFunctionContainer(const RFunctionContainer&);
        CFunctionContainer(const complex<double>&);
        CFunctionContainer(const double&);
        CFunctionContainer(const std::shared_ptr<ICFunction>&);
        CFunctionContainer& operator=(const CFunctionContainer&);
        CFunctionContainer& operator=(const RFunctionContainer&);
        CFunctionContainer& operator=(const complex<double>&);
        CFunctionContainer& operator=(const double&);

        CFunctionContainer operator-() const;

        CFunctionContainer operator+(const CFunctionContainer&) const;
        CFunctionContainer operator-(const CFunctionContainer&) const;
        CFunctionContainer operator*(const CFunctionContainer&) const;
        CFunctionContainer operator/(const CFunctionContainer&) const;
        CFunctionContainer& operator+=(const CFunctionContainer&);
        CFunctionContainer& operator-=(const CFunctionContainer&);
        CFunctionContainer& operator*=(const CFunctionContainer&);
        CFunctionContainer& operator/=(const CFunctionContainer&);

        CFunctionContainer operator+(const complex<double>&) const;
        CFunctionContainer operator-(const complex<double>&) const;
        CFunctionContainer operator*(const complex<double>&) const;
        CFunctionContainer operator/(const complex<double>&) const;
        CFunctionContainer& operator+=(const complex<double>&);
        CFunctionContainer& operator-=(const complex<double>&);
        CFunctionContainer& operator*=(const complex<double>&);
        CFunctionContainer& operator/=(const complex<double>&);

        CFunctionContainer operator+(const double&) const;
        CFunctionContainer operator-(const double&) const;
        CFunctionContainer operator*(const double&) const;
        CFunctionContainer operator/(const double&) const;
        CFunctionContainer& operator+=(const double&);
        CFunctionContainer& operator-=(const double&);
        CFunctionContainer& operator*=(const double&);
        CFunctionContainer& operator/=(const double&);

        CFunctionContainer operator+(const RFunctionContainer&) const;
        CFunctionContainer operator-(const RFunctionContainer&) const;
        CFunctionContainer operator*(const RFunctionContainer&) const;
        CFunctionContainer operator/(const RFunctionContainer&) const;
        CFunctionContainer& operator+=(const RFunctionContainer&);
        CFunctionContainer& operator-=(const RFunctionContainer&);
        CFunctionContainer& operator*=(const RFunctionContainer&);
        CFunctionContainer& operator/=(const RFunctionContainer&);
        const std::shared_ptr<ICFunction>& GetContent() const;
        std::string to_string()const;
        };

    class CFunctionVoid: public ICFunction
        {
    public:
        CFunctionVoid(){};
       ~CFunctionVoid(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };

    class CFunctionUnarMinus: public ICFunction
        {
    private:
        std::shared_ptr<ICFunction> argument;
    public:
        CFunctionUnarMinus(const std::shared_ptr<ICFunction>&);
        ~CFunctionUnarMinus(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };

    class CFunctionPlus: public ICFunction
        {
    private:
        std::shared_ptr<ICFunction> argument1;
        std::shared_ptr<ICFunction> argument2;
    public:
        CFunctionPlus(const std::shared_ptr<ICFunction>&,
                const std::shared_ptr<ICFunction>&);
        ~CFunctionPlus(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class CFunctionMinus: public ICFunction
        {
    private:
        std::shared_ptr<ICFunction> argument1;
        std::shared_ptr<ICFunction> argument2;
    public:
        CFunctionMinus(const std::shared_ptr<ICFunction>&,
                const std::shared_ptr<ICFunction>&);
        ~CFunctionMinus(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class CFunctionMultiply: public ICFunction
        {
    private:
        std::shared_ptr<ICFunction> argument1;
        std::shared_ptr<ICFunction> argument2;
    public:
        CFunctionMultiply(const std::shared_ptr<ICFunction>&,
                const std::shared_ptr<ICFunction>&);
        ~CFunctionMultiply(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class CFunctionDivide: public ICFunction
        {
    private:
        std::shared_ptr<ICFunction> argument1;
        std::shared_ptr<ICFunction> argument2;
    public:
        CFunctionDivide(const std::shared_ptr<ICFunction>&,
                const std::shared_ptr<ICFunction>&);
        ~CFunctionDivide(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };

    class CFunctionNumber: public ICFunction
        {
    private:
        complex<double> number;
    public:
        CFunctionNumber(const complex<double>&);
        CFunctionNumber(const double&);
        ~CFunctionNumber(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class CFunctionFunction: public ICFunction
        {
    private:
        std::shared_ptr<ICFunction> argument;
        std::string name;
    public:
        CFunctionFunction(const std::string&,
                const std::shared_ptr<ICFunction>&);
        ~CFunctionFunction(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class CFunctionRealFunction: public ICFunction
        {
    private:
        std::shared_ptr<IRFunction> argument;
    public:
        CFunctionRealFunction(const std::shared_ptr<IRFunction>&);
        ~CFunctionRealFunction(){};
        std::shared_ptr<ICFunction> copy(void) const override;
        std::string to_string(void) const override;
        };

    CFunctionContainer operator+(const double& in,const CFunctionContainer& in1);
    CFunctionContainer operator-(const double& in,const CFunctionContainer& in1);
    CFunctionContainer operator*(const double& in,const CFunctionContainer& in1);
    CFunctionContainer operator/(const double& in,const CFunctionContainer& in1);
    CFunctionContainer operator+(const complex<double>& in,const CFunctionContainer& in1);
    CFunctionContainer operator-(const complex<double>& in,const CFunctionContainer& in1);
    CFunctionContainer operator*(const complex<double>& in,const CFunctionContainer& in1);
    CFunctionContainer operator/(const complex<double>& in,const CFunctionContainer& in1);
    CFunctionContainer operator+(const RFunctionContainer& in,const CFunctionContainer& in1);
    CFunctionContainer operator-(const RFunctionContainer& in,const CFunctionContainer& in1);
    CFunctionContainer operator*(const RFunctionContainer& in,const CFunctionContainer& in1);
    CFunctionContainer operator/(const RFunctionContainer& in,const CFunctionContainer& in1);


    CFunctionContainer operator+(const complex<double>& in,const RFunctionContainer& in1);
    CFunctionContainer operator-(const complex<double>& in,const RFunctionContainer& in1);
    CFunctionContainer operator*(const complex<double>& in,const RFunctionContainer& in1);
    CFunctionContainer operator/(const complex<double>& in,const RFunctionContainer& in1);
    CFunctionContainer operator+(const RFunctionContainer& in,const complex<double>& in1);
    CFunctionContainer operator-(const RFunctionContainer& in,const complex<double>& in1);
    CFunctionContainer operator*(const RFunctionContainer& in,const complex<double>& in1);
    CFunctionContainer operator/(const RFunctionContainer& in,const complex<double>& in1);

    CFunctionContainer sin(const CFunctionContainer& in);
    CFunctionContainer cos(const CFunctionContainer& in);
    CFunctionContainer exp(const CFunctionContainer& in);
    CFunctionContainer conj(const CFunctionContainer& in);
    };


#endif /*__complex_function_hpp_*/
