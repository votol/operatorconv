#include <typeinfo>
#include "const_operators.hpp"


using namespace Abstr;

/// General Functions

/// Getting type of signature intersection


IntersectionResult get_intersection(const std::set<unsigned int>& object,const std::set<unsigned int>& sample)
    {
    IntersectionResult res;
    std::set<unsigned int> tmp_sig=sample;
    for(auto it=object.begin();it!=object.end();++it)
        tmp_sig.erase(*it);

    if(tmp_sig.size()==0)
        {
        res.type=IntersectionType::non;
        }
    else if(*object.begin()>*tmp_sig.rbegin())
        {
        res.dimension.resize(2);
        res.dimension[0]=1;
        for(auto it=tmp_sig.begin();it!=tmp_sig.end();++it)
            res.dimension[0]*=Geometry::GetInstance().GetDimension(*it);
        res.type=IntersectionType::left;
        res.sig.resize(4);
        res.sig[0]=object;
        res.sig[0].insert(tmp_sig.begin(),tmp_sig.end());
        res.sig[1]=object;
        res.sig[2]=res.sig[0];
        }
    else if(*object.rbegin()<*tmp_sig.begin())
        {
        res.dimension.resize(2);
        res.dimension[1]=1;
        for(auto it=tmp_sig.begin();it!=tmp_sig.end();++it)
            res.dimension[1]*=Geometry::GetInstance().GetDimension(*it);
        res.type=IntersectionType::right;
        res.sig.resize(4);
        res.sig[0]=object;
        res.sig[0].insert(tmp_sig.begin(),tmp_sig.end());
        res.sig[1]=object;
        res.sig[3]=res.sig[0];
        }
    else if(*(--tmp_sig.lower_bound(*object.rbegin()))<*object.begin())
        {
        res.dimension.resize(2);
        res.dimension[0]=1;
        auto it=tmp_sig.begin();
        while(*it<*object.begin())
            {
            res.dimension[0]*=Geometry::GetInstance().GetDimension(*it);
            ++it;
            }
        res.dimension[1]=1;
        while(it!=tmp_sig.end())
            {
            res.dimension[1]*=Geometry::GetInstance().GetDimension(*it);
            ++it;
            }
        res.type=IntersectionType::both;
        res.sig.resize(4);
        res.sig[0]=object;
        res.sig[0].insert(tmp_sig.begin(),tmp_sig.end());
        res.sig[1]=object;
        res.sig[2]=object;
        res.sig[2].insert(tmp_sig.begin(),tmp_sig.lower_bound(*object.rbegin()));
        res.sig[3]=object;
        res.sig[3].insert(tmp_sig.lower_bound(*object.rbegin()),tmp_sig.end());
        }
    else
        {
        res.type=IntersectionType::mixed;
        res.dimension.resize(2);
        res.dimension[0]=1;
        for(auto it=tmp_sig.begin();it!=tmp_sig.lower_bound(*object.begin());++it)
            {
            res.dimension[0]*=Geometry::GetInstance().GetDimension(*it);
            }
        res.dimension[1]=1;
        for(auto it=tmp_sig.lower_bound(*object.rbegin());it!=tmp_sig.end();++it)
            {
            res.dimension[1]*=Geometry::GetInstance().GetDimension(*it);
            }
        res.sig.resize(4);
        res.sig[0]=object;
        res.sig[0].insert(tmp_sig.begin(),tmp_sig.end());
        res.sig[1]=object;
        res.sig[1].insert(tmp_sig.lower_bound(*object.begin()),tmp_sig.lower_bound(*object.rbegin()));
        res.sig[2]=object;
        res.sig[2].insert(tmp_sig.begin(),tmp_sig.lower_bound(*object.rbegin()));
        res.sig[3]=object;
        res.sig[3].insert(tmp_sig.lower_bound(*object.begin()),tmp_sig.end());
        }
    return res;
    }

/// leading signatures

void lead_signature(std::shared_ptr<BaseMatrix>& in,const std::set<unsigned int>& signature)
    {
    if(signature!=in->GetSignature())
        {
        auto tmp=get_intersection(in->GetSignature(),signature);
        switch(tmp.type)
            {
        case IntersectionType::left:
            in=std::make_shared<TensorLeadMatrix>(in,tmp.dimension[0],
                    TensorLeadMatrix::options::left,tmp.sig[0]);
            break;
        case IntersectionType::right:
            in=std::make_shared<TensorLeadMatrix>(in,tmp.dimension[1],
                    TensorLeadMatrix::options::right,tmp.sig[0]);
            break;
        case IntersectionType::both:
            in=std::make_shared<TensorLeadMatrix>(in,tmp.dimension[0],
                    TensorLeadMatrix::options::left,tmp.sig[2]);
            in=std::make_shared<TensorLeadMatrix>(in,tmp.dimension[1],
                    TensorLeadMatrix::options::right,tmp.sig[0]);
            break;
        case IntersectionType::mixed:
            in->lead(tmp.sig[1]);
            if(tmp.dimension[0]!=1)
                {
                in=std::make_shared<TensorLeadMatrix>(in,tmp.dimension[0],
                        TensorLeadMatrix::options::left,tmp.sig[2]);
                }
            if(tmp.dimension[1]!=1)
                {
                in=std::make_shared<TensorLeadMatrix>(in,tmp.dimension[1],
                        TensorLeadMatrix::options::right,tmp.sig[0]);
                }
            break;
        default:
            throw std::runtime_error("Can't lead signatures");
            }
        }
    }
/// print signature

void print_signature(const std::set<unsigned int>& in)
    {
    for(auto it=in.begin();it!=in.end();++it)
        std::cout<<*it<<" ";
    std::cout<<std::endl;
    }
///base matrix class
std::set<unsigned int>& BaseMatrix::GetSignature(void)
    {
    return signature;
    }

/// matrix container class
MatrixContainer::MatrixContainer(void)
    {
    child=std::make_shared<VoidMatrix>();
    }

MatrixContainer::MatrixContainer(const MatrixContainer& in)
    {
    child=in.child->copy();
    }
MatrixContainer::MatrixContainer(const std::shared_ptr<BaseMatrix>&in):child(in)
    {

    }

MatrixContainer& MatrixContainer::operator= (const MatrixContainer& in)
    {
    child=in.child->copy();
    return *this;
    }

MatrixContainer MatrixContainer::operator- (void)const
    {
    MatrixContainer res;
    res.child=std::make_shared<UnarMinusMatrix>(child);
    return res;
    }

MatrixContainer MatrixContainer::operator+ (const MatrixContainer& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<PlusMatrix>(child->copy(),in.child->copy());
    return res;
    }

MatrixContainer MatrixContainer::operator- (const MatrixContainer& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<MinusMatrix>(child,in.child);
    return res;
    }

MatrixContainer MatrixContainer::operator* (const MatrixContainer& in)const
    {
    MatrixContainer res;
    if(*(child->GetSignature().rbegin())<*(in.child->GetSignature().begin()))
        res.child=std::make_shared<MultiplyTensMatrix>(child,in.child);
    else if(*(child->GetSignature().begin())>*(in.child->GetSignature().rbegin()))
        res.child=std::make_shared<MultiplyTensMatrix>(in.child,child);
    else
        res.child=std::make_shared<MultiplyMatrix>(child,in.child);
    return res;
    }

MatrixContainer& MatrixContainer::operator+= (const MatrixContainer& in)
    {
    child=std::make_shared<PlusMatrix>(child->copy(),in.child->copy());
    return *this;
    }

MatrixContainer& MatrixContainer::operator-= (const MatrixContainer& in)
    {
    child=std::make_shared<MinusMatrix>(child->copy(),in.child->copy());
    return *this;
    }

MatrixContainer& MatrixContainer::operator*= (const MatrixContainer& in)
    {
    if(*(child->GetSignature().rbegin())<*(in.child->GetSignature().begin()))
        child=std::make_shared<MultiplyTensMatrix>(child,in.child);
    else if(*(child->GetSignature().begin())>*(in.child->GetSignature().rbegin()))
        child=std::make_shared<MultiplyTensMatrix>(in.child,child);
    else
        child=std::make_shared<MultiplyMatrix>(child,in.child);
    return *this;
    }

MatrixContainer MatrixContainer::operator+ (const complex<double>& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<PlusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(in,child->GetSignature()));
    return res;
    }
MatrixContainer MatrixContainer::operator- (const complex<double>& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<MinusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(in,child->GetSignature()));
    return res;
    }
MatrixContainer MatrixContainer::operator* (const complex<double>& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<MultiplyNumberMatrix>(child->copy(),in);
    return res;
    }

MatrixContainer MatrixContainer::operator/ (const complex<double>& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<MultiplyNumberMatrix>(child->copy(),complex<double>(1.0)/in);
    return res;
    }

MatrixContainer& MatrixContainer::operator+= (const complex<double>& in)
    {
    child=std::make_shared<PlusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(in,child->GetSignature()));
    return *this;
    }

MatrixContainer& MatrixContainer::operator-= (const complex<double>& in)
    {
    child=std::make_shared<MinusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(in,child->GetSignature()));
    return *this;
    }

MatrixContainer& MatrixContainer::operator*= (const complex<double>& in)
    {
    child=std::make_shared<MultiplyNumberMatrix>(child->copy(),in);
    return *this;
    }

MatrixContainer& MatrixContainer::operator/= (const complex<double>& in)
    {
    child=std::make_shared<MultiplyNumberMatrix>(child->copy(),complex<double>(1.0)/in);
    return *this;
    }


MatrixContainer MatrixContainer::operator+ (const double& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<PlusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(complex<double>(in),child->GetSignature()));
    return res;
    }
MatrixContainer MatrixContainer::operator- (const double& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<MinusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(complex<double>(in),child->GetSignature()));
    return res;
    }
MatrixContainer MatrixContainer::operator* (const double& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<MultiplyNumberMatrix>(child->copy(),complex<double>(in));
    return res;
    }

MatrixContainer MatrixContainer::operator/ (const double& in)const
    {
    MatrixContainer res;
    res.child=std::make_shared<MultiplyNumberMatrix>(child->copy(),complex<double>(1.0/in));
    return res;
    }

MatrixContainer& MatrixContainer::operator+= (const double& in)
    {
    child=std::make_shared<PlusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(complex<double>(in),child->GetSignature()));
    return *this;
    }

MatrixContainer& MatrixContainer::operator-= (const double& in)
    {
    child=std::make_shared<MinusMatrix>(child->copy(),std::make_shared<DiagonalMatrix>(complex<double>(in),child->GetSignature()));
    return *this;
    }

MatrixContainer& MatrixContainer::operator*= (const double& in)
    {
    child=std::make_shared<MultiplyNumberMatrix>(child->copy(),complex<double>(in));
    return *this;
    }

MatrixContainer& MatrixContainer::operator/= (const double& in)
    {
    child=std::make_shared<MultiplyNumberMatrix>(child->copy(),complex<double>(1.0/in));
    return *this;
    }

MatrixImpl MatrixContainer::calculate(void)
    {
    std::set<unsigned int> geometry_signature;
    for(unsigned int peri=0;peri<Geometry::GetInstance().GetNumberOfParticles();peri++)
        {
        geometry_signature.insert(peri);
        }
    lead_signature(child,geometry_signature);
    return child->calculate();
    }

void MatrixContainer::print(void)
    {
    std::set<unsigned int> geometry_signature;
    for(unsigned int peri=0;peri<Geometry::GetInstance().GetNumberOfParticles();peri++)
        {
        geometry_signature.insert(peri);
        }
    lead_signature(child,geometry_signature);
    child->print();
    }

MatrixContainer MatrixContainer::conjugation(void) const
    {
    return MatrixContainer(child->conjugation());
    }

MatrixContainer Abstr::operator+(const complex<double>& a,const MatrixContainer& b)
    {
    return b+a;
    }
MatrixContainer Abstr::operator-(const complex<double>& a,const MatrixContainer& b)
    {
    return -b+a;
    }
MatrixContainer Abstr::operator*(const complex<double>& a,const MatrixContainer& b)
    {
    return b*a;
    }

MatrixContainer Abstr::operator+(const double& a,const MatrixContainer& b)
    {
    return b+a;
    }
MatrixContainer Abstr::operator-(const double& a,const MatrixContainer& b)
    {
    return -b+a;
    }
MatrixContainer Abstr::operator*(const double& a,const MatrixContainer& b)
    {
    return b*a;
    }

MatrixContainer Abstr::conj(const MatrixContainer& in)
    {
    return in.conjugation();
    }



/// void matrix

std::shared_ptr<BaseMatrix> VoidMatrix::copy(void) const
    {
    return std::make_shared<VoidMatrix>();
    }

MatrixImpl VoidMatrix::calculate(void) const
    {
    throw std::runtime_error("Trying to calculate void matrix");
    }

void VoidMatrix::lead(const std::set<unsigned int>&)
    {
    throw std::runtime_error("Can't change void matrix");
    }
void VoidMatrix::print(void)
    {
    throw std::runtime_error("Trying to calculate void matrix");
    }
std::shared_ptr<BaseMatrix> VoidMatrix::conjugation(void)
    {
    throw std::runtime_error("Trying to conduct conjugation of void matrix");
    }

/// unary minus matrix
UnarMinusMatrix::UnarMinusMatrix(const std::shared_ptr<BaseMatrix>& in)
    {
    arg=in;
    signature=in->GetSignature();
    }

std::shared_ptr<BaseMatrix> UnarMinusMatrix::copy(void) const
    {
    return std::make_shared<UnarMinusMatrix>(arg);
    }

MatrixImpl UnarMinusMatrix::calculate(void) const
    {
    return -(arg->calculate());
    }

void UnarMinusMatrix::lead(const std::set<unsigned int>& in)
    {
    lead_signature(arg,in);
    signature=arg->GetSignature();
    }
void UnarMinusMatrix::print(void)
    {
    std::cout<<"UnarMinus\n";
    print_signature(signature);
    print_signature(arg->GetSignature());
    std::cout<<"******************************************\n";
    arg->print();
    }
std::shared_ptr<BaseMatrix> UnarMinusMatrix::conjugation(void)
    {
    return std::make_shared<UnarMinusMatrix>(arg->conjugation());
    }

/// plus matrix
PlusMatrix::PlusMatrix(const std::shared_ptr<BaseMatrix>& a,
        const std::shared_ptr<BaseMatrix>& b):first(a),second(b)
    {
    signature=first->GetSignature();
    signature.insert(second->GetSignature().begin(),
            second->GetSignature().end());
    lead_signature(first,signature);
    lead_signature(second,signature);
    }
MatrixImpl PlusMatrix::calculate(void) const
    {
    return first->calculate()+second->calculate();
    }

std::shared_ptr<BaseMatrix> PlusMatrix::copy(void) const
    {
    return std::make_shared<PlusMatrix>(first,second);
    }
void PlusMatrix::lead(const std::set<unsigned int>& in)
    {
    signature.insert(in.begin(),in.end());
    lead_signature(first,signature);
    lead_signature(second,signature);
    }
void PlusMatrix::print(void)
    {
    std::cout<<"+\n";
    print_signature(signature);
    print_signature(first->GetSignature());
    print_signature(second->GetSignature());
    std::cout<<"******************************************\n";
    first->print();
    second->print();
    }
std::shared_ptr<BaseMatrix> PlusMatrix::conjugation(void)
    {
    return std::make_shared<PlusMatrix>(first->conjugation(),second->conjugation());
    }
/// minus matrix
MinusMatrix::MinusMatrix(const std::shared_ptr<BaseMatrix>& a,
        const std::shared_ptr<BaseMatrix>& b):first(a),second(b)
    {
    signature=first->GetSignature();
    signature.insert(second->GetSignature().begin(),
            second->GetSignature().end());
    lead_signature(first,signature);
    lead_signature(second,signature);
    }
MatrixImpl MinusMatrix::calculate(void) const
    {
    return first->calculate()-second->calculate();
    }

std::shared_ptr<BaseMatrix> MinusMatrix::copy(void) const
    {
    return std::make_shared<MinusMatrix>(first,second);
    }
void MinusMatrix::lead(const std::set<unsigned int>& in)
    {
    signature.insert(in.begin(),in.end());
    lead_signature(first,signature);
    lead_signature(second,signature);
    }
void MinusMatrix::print(void)
    {
    std::cout<<"-\n";
    print_signature(signature);
    print_signature(first->GetSignature());
    print_signature(second->GetSignature());
    std::cout<<"******************************************\n";
    first->print();
    second->print();
    }
std::shared_ptr<BaseMatrix> MinusMatrix::conjugation(void)
    {
    return std::make_shared<MinusMatrix>(first->conjugation(),second->conjugation());
    }
/// multiply matrix
MultiplyMatrix::MultiplyMatrix(const std::shared_ptr<BaseMatrix>& a,
        const std::shared_ptr<BaseMatrix>& b):first(a),second(b)
    {
    signature=first->GetSignature();
    signature.insert(second->GetSignature().begin(),
            second->GetSignature().end());
    lead_signature(first,signature);
    lead_signature(second,signature);
    }
MatrixImpl MultiplyMatrix::calculate(void) const
    {
    return first->calculate()*second->calculate();
    }

std::shared_ptr<BaseMatrix> MultiplyMatrix::copy(void) const
    {
    return std::make_shared<MultiplyMatrix>(first,second);
    }
void MultiplyMatrix::lead(const std::set<unsigned int>& in)
    {
    signature.insert(in.begin(),in.end());
    lead_signature(first,signature);
    lead_signature(second,signature);
    }
void MultiplyMatrix::print(void)
    {
    std::cout<<"*\n";
    print_signature(signature);
    print_signature(first->GetSignature());
    print_signature(second->GetSignature());
    std::cout<<"******************************************\n";
    first->print();
    second->print();
    }
std::shared_ptr<BaseMatrix> MultiplyMatrix::conjugation(void)
    {
    return std::make_shared<MultiplyMatrix>(second->conjugation(),first->conjugation());
    }




/// multiply tensor matrix
unsigned int MultiplyTensMatrix::dimension(const std::set<unsigned int>& in)
    {
    unsigned int res=1;
    for(auto it=in.begin();it!=in.end();++it)
        res*=Geometry::GetInstance().GetDimension(*it);
    return res;
    }
MultiplyTensMatrix::MultiplyTensMatrix(const std::shared_ptr<BaseMatrix>& a,
        const std::shared_ptr<BaseMatrix>& b):first(a),second(b)
    {
    signature=first->GetSignature();
    signature.insert(second->GetSignature().begin(),
            second->GetSignature().end());
    }
MatrixImpl MultiplyTensMatrix::calculate(void) const
    {
    return MatrixTensorMul(first->calculate(),second->calculate());
    }

std::shared_ptr<BaseMatrix> MultiplyTensMatrix::copy(void) const
    {
    return std::make_shared<MultiplyTensMatrix>(first,second);
    }
void MultiplyTensMatrix::lead(const std::set<unsigned int>& in)
    {
    signature.insert(in.begin(),in.end());
    std::set<unsigned int> tmp_sig_l,tmp_sig_r;
    if(dimension(first->GetSignature())>dimension(second->GetSignature()))
        {
        tmp_sig_l.insert(signature.begin(),
                signature.upper_bound(*first->GetSignature().rbegin()));
        tmp_sig_r.insert(signature.upper_bound(*first->GetSignature().rbegin()),
                signature.end());
        }
    else
        {
        tmp_sig_l.insert(signature.begin(),
                signature.lower_bound(*second->GetSignature().begin()));
        tmp_sig_r.insert(signature.lower_bound(*second->GetSignature().begin()),
                signature.end());
        }
    lead_signature(first,tmp_sig_l);
    lead_signature(second,tmp_sig_r);
    }
void MultiplyTensMatrix::print(void)
    {
    std::cout<<"*t\n";
    print_signature(signature);
    print_signature(first->GetSignature());
    print_signature(second->GetSignature());
    std::cout<<"******************************************\n";
    first->print();
    second->print();
    }
std::shared_ptr<BaseMatrix> MultiplyTensMatrix::conjugation(void)
    {
    return std::make_shared<MultiplyTensMatrix>(first->conjugation(),second->conjugation());
    }
///Multiplication of matrix on number

MultiplyNumberMatrix::MultiplyNumberMatrix(const std::shared_ptr<BaseMatrix>& matr,const complex<double>& num):first(matr),number(num)
    {
    signature=first->GetSignature();
    }
std::shared_ptr<BaseMatrix> MultiplyNumberMatrix::copy(void) const
    {
    return std::make_shared<MultiplyNumberMatrix>(first,number);
    }

MatrixImpl MultiplyNumberMatrix::calculate(void) const
    {
    return first->calculate()*number;
    }

void MultiplyNumberMatrix::lead(const std::set<unsigned int>& in)
    {
    first->lead(in);
    signature=in;
    }

void MultiplyNumberMatrix::print(void)
    {
    std::cout<<"*"<<number<<"\n";
    print_signature(signature);
    print_signature(first->GetSignature());
    std::cout<<"******************************************\n";
    first->print();
    }

std::shared_ptr<BaseMatrix> MultiplyNumberMatrix::conjugation(void)
    {
    return std::make_shared<MultiplyNumberMatrix>(first->conjugation(),conj(number));
    }

/// diagonal matrix

DiagonalMatrix::DiagonalMatrix(const complex<double>& in):number(in)
    {
    for(unsigned int peri=0;peri<Geometry::GetInstance().GetNumberOfParticles();peri++)
        {
        signature.insert(peri);
        }
    }

DiagonalMatrix::DiagonalMatrix(const complex<double>& num,const std::set<unsigned int>& sig):number(num)
    {
    signature=sig;
    }

std::shared_ptr<BaseMatrix> DiagonalMatrix::copy(void) const
    {
    return std::make_shared<DiagonalMatrix>(number,signature);
    }

MatrixImpl DiagonalMatrix::calculate(void) const
    {
    unsigned int dimension=1;
    for(unsigned int peri=0;peri<Geometry::GetInstance().GetNumberOfParticles();peri++)
        {
        dimension*=Geometry::GetInstance().GetDimension(peri);
        }
    MatrixImpl matr(dimension,dimension,dimension);
    for(unsigned int peri1=0;peri1<dimension;++peri1)
        SetValue(matr,peri1,peri1,number);
    return matr;
    }

void DiagonalMatrix::lead(const std::set<unsigned int>& sig)
    {
    signature=sig;
    }

void DiagonalMatrix::print(void)
    {
    std::cout<<"diagonal: "<<number<<"\n";
    print_signature(signature);
    std::cout<<"******************************************\n";
    }
std::shared_ptr<BaseMatrix> DiagonalMatrix::conjugation(void)
    {
    return std::make_shared<DiagonalMatrix>(conj(number),signature);
    }

/// tensor lead matrix
TensorLeadMatrix::OptionsPair::OptionsPair(const IntersectionType& f,const IntersectionType& s):first(f),second(s)
    {

    }
TensorLeadMatrix::OptionsPair::OptionsPair(
        const TensorLeadMatrix::OptionsPair& in):first(in.first),second(in.second)
    {

    }
TensorLeadMatrix::OptionsPair& TensorLeadMatrix::OptionsPair::operator=
        (const TensorLeadMatrix::OptionsPair& in)
    {
    first=in.first;
    second=in.second;
    return *this;
    }

bool TensorLeadMatrix::OptionsPairCompare::operator()(const OptionsPair& a,const OptionsPair& b)
    {
    if(a.first!=b.first)
        return a.first<b.first;
    else
        return a.second<b.second;
    }

void TensorLeadMatrix::ll(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::left:
        dimension*=high.dimension[0];
        break;
    default:
        std::runtime_error("Something wrong in trying to lead  tensor matrix: ll");
        }

    }

void TensorLeadMatrix::lb(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::right:
        arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[0],
                TensorLeadMatrix::options::left,low.sig[2]);
        break;
    default:
        std::runtime_error("Something wrong in trying to lead  tensor matrix: lb");
        }

    }

void TensorLeadMatrix::rr(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::right:
        dimension*=high.dimension[1];
        break;
    default:
        std::runtime_error("Something wrong in trying to lead  tensor matrix: rr");
        }

    }

void TensorLeadMatrix::rb(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::left:
        arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[1],
                TensorLeadMatrix::options::right,low.sig[3]);
        break;
    default:
        std::runtime_error("Something wrong in trying to lead  tensor matrix: rb");
        }

    }

void TensorLeadMatrix::bb(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::left:
        dimension*=high.dimension[0];
        arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[1],
                TensorLeadMatrix::options::right,low.sig[3]);
        break;
    case options::right:
        dimension*=high.dimension[1];
        arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[0],
                TensorLeadMatrix::options::left,low.sig[2]);
        break;
        }
    }

void TensorLeadMatrix::ml(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::left:
        dimension=low.dimension[0];
        break;
    default:
        std::runtime_error("Something wrong in trying to lead  tensor matrix: rb");
        }

    }

void TensorLeadMatrix::mr(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::right:
        dimension=low.dimension[1];
        break;
    default:
        std::runtime_error("Something wrong in trying to lead  tensor matrix: rb");
        }

    }

void TensorLeadMatrix::mb(const IntersectionResult& high,const IntersectionResult& low)
    {
    switch(opt)
        {
    case options::left:
        dimension=low.dimension[0];
        arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[1],
                TensorLeadMatrix::options::right,low.sig[3]);
        break;
    case options::right:
        dimension=low.dimension[1];
        arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[0],
                TensorLeadMatrix::options::left,low.sig[2]);
        break;
        }
    }

void TensorLeadMatrix::mm(const IntersectionResult& high,const IntersectionResult& low)
    {
    arg->lead(low.sig[1]);
    switch(opt)
        {
    case options::left:
        dimension=low.dimension[0];
        if(low.dimension[1]!=1)
            {
            arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[1],
                    TensorLeadMatrix::options::right,low.sig[3]);
            }
        break;
    case options::right:
        dimension=low.dimension[1];
        if(low.dimension[0]!=1)
            {
            arg=std::make_shared<TensorLeadMatrix>(arg,low.dimension[0],
                    TensorLeadMatrix::options::left,low.sig[2]);
            }
        break;
        }
    }

TensorLeadMatrix::TensorLeadMatrix(const std::shared_ptr<BaseMatrix>& in,unsigned int dim,const options& o,
                const std::set<unsigned int> sig):arg(in),dimension(dim),opt(o)
    {
    signature=sig;
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::left,IntersectionType::left),
            std::bind(&TensorLeadMatrix::ll,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::left,IntersectionType::both),
            std::bind(&TensorLeadMatrix::lb,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::right,IntersectionType::right),
            std::bind(&TensorLeadMatrix::rr,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::right,IntersectionType::both),
            std::bind(&TensorLeadMatrix::rb,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::both,IntersectionType::both),
            std::bind(&TensorLeadMatrix::bb,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::mixed,IntersectionType::left),
            std::bind(&TensorLeadMatrix::ml,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::mixed,IntersectionType::right),
            std::bind(&TensorLeadMatrix::mr,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::mixed,IntersectionType::both),
            std::bind(&TensorLeadMatrix::mb,this,std::placeholders::_1,std::placeholders::_2)));
    lead_functions.insert(std::pair<OptionsPair,LeadFunction>(
            OptionsPair(IntersectionType::mixed,IntersectionType::mixed),
            std::bind(&TensorLeadMatrix::mm,this,std::placeholders::_1,std::placeholders::_2)));

    }

std::shared_ptr<BaseMatrix> TensorLeadMatrix::copy(void) const
    {
    return std::make_shared<TensorLeadMatrix>(arg,dimension,opt,signature);
    }
MatrixImpl TensorLeadMatrix::calculate(void) const
    {
    MatrixImpl res=arg->calculate();
    switch(opt)
        {
    case options::left:
        res=MatrixExtend(dimension,res);
        break;
    case options::right:
        res=MatrixExtend(res,dimension);
        break;
        }
    return res;
    }
void TensorLeadMatrix::lead(const std::set<unsigned int>& in)
    {
    if(signature!=in)
        {
        auto tmp=get_intersection(signature,in);
        auto tmp1=get_intersection(arg->GetSignature(),in);
        lead_functions.find(OptionsPair(tmp.type,tmp1.type))->second(tmp,tmp1);
        signature=tmp.sig[0];
        }
    }
void TensorLeadMatrix::print(void)
    {
    std::cout<<"Tensor_lead ";
    switch(opt)
        {
    case options::left:
        std::cout<<"left ";
        break;
    case options::right:
        std::cout<<"right ";
        break;
        }
    std::cout<<dimension<<std::endl;
    print_signature(signature);
    print_signature(arg->GetSignature());
    std::cout<<"******************************************\n";
    arg->print();
    }

std::shared_ptr<BaseMatrix> TensorLeadMatrix::conjugation(void)
    {
    return std::make_shared<TensorLeadMatrix>(arg->conjugation(),dimension,opt,signature);
    }

///a matrix
Matrix_a::Matrix_a(unsigned int p_num):particle_number(p_num)
    {
    signature.insert(particle_number);
    }

std::shared_ptr<BaseMatrix> Matrix_a::copy(void) const
    {
    return std::make_shared<Matrix_a>(particle_number);
    }
MatrixImpl Matrix_a::calculate(void) const
    {
    MatrixImpl matr(Geometry::GetInstance().GetDimension(particle_number),
            Geometry::GetInstance().GetDimension(particle_number),
            Geometry::GetInstance().GetDimension(particle_number)-1);
    for(unsigned int peri1=1;peri1<Geometry::GetInstance().GetDimension(particle_number);++peri1)
        SetValue(matr,peri1-1,peri1,complex<double>(sqrt(double(peri1))));
    return matr;
    }
void Matrix_a::lead(const std::set<unsigned int>&in)
    {
    throw std::runtime_error("Can't lead matrix");
    }
void Matrix_a::print(void)
    {
    std::cout<<"a\n";
    print_signature(signature);
    std::cout<<"******************************************\n";
    }
std::shared_ptr<BaseMatrix> Matrix_a::conjugation(void)
    {
    return std::make_shared<Matrix_ak>(particle_number);
    }


///ak matrix
Matrix_ak::Matrix_ak(unsigned int p_num):particle_number(p_num)
    {
    signature.insert(particle_number);
    }

std::shared_ptr<BaseMatrix> Matrix_ak::copy(void) const
    {
    return std::make_shared<Matrix_ak>(particle_number);
    }
MatrixImpl Matrix_ak::calculate(void) const
    {
    MatrixImpl matr(Geometry::GetInstance().GetDimension(particle_number),
            Geometry::GetInstance().GetDimension(particle_number),
            Geometry::GetInstance().GetDimension(particle_number)-1);
    for(unsigned int peri1=1;peri1<Geometry::GetInstance().GetDimension(particle_number);++peri1)
        SetValue(matr,peri1,peri1-1,complex<double>(sqrt(double(peri1))));
    return matr;
    }
void Matrix_ak::lead(const std::set<unsigned int>&in)
    {
    throw std::runtime_error("Can't lead matrix");
    }

void Matrix_ak::print(void)
    {
    std::cout<<"ak\n";
    print_signature(signature);
    std::cout<<"******************************************\n";
    }
std::shared_ptr<BaseMatrix> Matrix_ak::conjugation(void)
    {
    return std::make_shared<Matrix_a>(particle_number);
    }

///sig matrix
Matrix_sig::Matrix_sig(unsigned int p_num,unsigned int from,unsigned int to):particle_number(p_num),f(from),t(to)
    {
    signature.insert(particle_number);
    }

std::shared_ptr<BaseMatrix> Matrix_sig::copy(void) const
    {
    return std::make_shared<Matrix_sig>(particle_number,f,t);
    }
MatrixImpl Matrix_sig::calculate(void) const
    {
    MatrixImpl matr(Geometry::GetInstance().GetDimension(particle_number),
            Geometry::GetInstance().GetDimension(particle_number),1);
    SetValue(matr,t,f,complex<double>(1.0));
    return matr;
    }
void Matrix_sig::lead(const std::set<unsigned int>&in)
    {
    throw std::runtime_error("Can't lead matrix");
    }
void Matrix_sig::print(void)
    {
    std::cout<<"sig "<<f<<" "<<t<<std::endl;
    print_signature(signature);
    std::cout<<"******************************************\n";
    }
std::shared_ptr<BaseMatrix> Matrix_sig::conjugation(void)
    {
    return std::make_shared<Matrix_sig>(particle_number,t,f);
    }

/// Particle class
Particle::Particle(const unsigned int& in)
    {
    if(in>Geometry::GetInstance().GetNumberOfParticles())
        throw std::invalid_argument("Trying to access non existing particle");
    number=in;
    }
MatrixContainer Particle::a()
    {
    return MatrixContainer(std::make_shared<Matrix_a>(number));
    }
MatrixContainer Particle::ak()
    {
    return MatrixContainer(std::make_shared<Matrix_ak>(number));
    }
MatrixContainer Particle::sig(const unsigned int& from,const unsigned int to)
    {
    return MatrixContainer(std::make_shared<Matrix_sig>(number,from,to));
    }

///Particles container class
Particle ParticlesContainer::operator()(const unsigned int& in)
    {
    return Particle(in);
    }
ParticlesContainer& ParticlesContainer::GetInstance()
    {
    static ParticlesContainer pc;
    return pc;
    }
