#include "matrix.hpp"

using namespace Abstr;


void Abstr::SetValue(MatrixImpl& in,unsigned int row,unsigned int col,complex<double> val)
    {
    in.insert_element(row,col,val);
    }
MatrixImpl Abstr::MatrixExtend(const unsigned int dim,const MatrixImpl& a)
    {
    MatrixImpl res(a.size1()*dim,a.size2()*dim);
    for(auto it1=a.begin1();it1!=a.end1();++it1)
        {
        for(unsigned int peri=0;peri<dim;peri++)
            {
            for(auto it2=it1.begin();it2!=it1.end();++it2)
                {
                res.insert_element(it2.index1()*dim+peri,it2.index2()*dim+peri,*it2);
                }
            }
        }
    return res;
    }

MatrixImpl Abstr::MatrixExtend(const MatrixImpl& a,const unsigned int dim)
    {
    MatrixImpl res(a.size1()*dim,a.size2()*dim);
    for(unsigned int peri=0;peri<dim;peri++)
        {
        for(auto it1=a.begin1();it1!=a.end1();++it1)
            {
            for(auto it2=it1.begin();it2!=it1.end();++it2)
                {
                res.insert_element(it2.index1()+peri*a.size1(),it2.index2()+peri*a.size2(),*it2);
                }
            }
        }
    return res;
    }

MatrixImpl Abstr::operator*(const MatrixImpl& a,const MatrixImpl& b)
    {
    return boost::numeric::ublas::prod(a,b);
    }

MatrixImpl Abstr::MatrixTensorMul(const MatrixImpl& a,const MatrixImpl& b)
    {
    MatrixImpl res(a.size1()*b.size1(),a.size2()*b.size2());
    for(auto it_b1=b.begin1();it_b1!=b.end1();++it_b1)
        {
        for(auto it_a1=a.begin1();it_a1!=a.end1();++it_a1)
            {
            for(auto it_b2=it_b1.begin();it_b2!=it_b1.end();++it_b2)
                {
                for(auto it_a2=it_a1.begin();it_a2!=it_a1.end();++it_a2)
                    {
                    res.insert_element(it_b2.index1()*a.size1()+it_a2.index1(),
                            it_b2.index2()*a.size2()+it_a2.index2(),
                            (*it_a2)*(*it_b2));
                    }
                }
            }
        }
    return res;
    }
std::ostream& Abstr::operator<<(std::ostream& fs,const MatrixImpl& in)
    {
    unsigned int size=0;;
    for(auto it1=in.begin1();it1!=in.end1();++it1)
        {
        for(auto it2=it1.begin();it2!=it1.end();++it2)
            {
            size++;
            }
        }
    std::vector<complex<double> > data(size);
    std::vector<unsigned int > row(size);
    std::vector<unsigned int > col(size);
    auto data_it=data.begin();
    auto row_it=row.begin();
    auto col_it=col.begin();
    for(auto it1=in.begin1();it1!=in.end1();++it1)
        {
        for(auto it2=it1.begin();it2!=it1.end();++it2)
            {
            *data_it=*it2;
            *row_it=it2.index1();
            *col_it=it2.index2();
            ++data_it;
            ++row_it;
            ++col_it;
            }
        }
    fs.write((const char *)&size,4);
    fs.write((const char *)row.data(),4*size);
    fs.write((const char *)col.data(),4*size);
    fs.write((const char *)data.data(),16*size);
    return fs;
    }
