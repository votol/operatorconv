#include "real_function.hpp"

using namespace Abstr;

RFunctionContainer::RFunctionContainer(void)
    {
    content = std::shared_ptr<IRFunction>(new RFunctionVoid);
    }

RFunctionContainer::RFunctionContainer(const RFunctionContainer& in)
    {
    content = in.content->copy();
    }

RFunctionContainer::RFunctionContainer(const std::shared_ptr<IRFunction>& in):content(in)
    {

    }

RFunctionContainer& RFunctionContainer::operator =(const RFunctionContainer& in)
    {
    content = in.content->copy();
    return *this;
    }
RFunctionContainer& RFunctionContainer::operator =(const double& in)
    {
    content = std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in));
    return *this;
    }

RFunctionContainer RFunctionContainer::operator -()const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionUnarMinus(content->copy()));
    return tmp;
    }
RFunctionContainer RFunctionContainer::operator +(const RFunctionContainer& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionPlus(content->copy(), in.content->copy()));
    return tmp;
    }
RFunctionContainer RFunctionContainer::operator -(const RFunctionContainer& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionMinus(content->copy(), in.content->copy()));
    return tmp;
    }
RFunctionContainer RFunctionContainer::operator *(const RFunctionContainer& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionMultiply(content->copy(), in.content->copy()));
    return tmp;
    }
RFunctionContainer RFunctionContainer::operator /(const RFunctionContainer& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionDivide(content->copy(), in.content->copy()));
    return tmp;
    }

RFunctionContainer& RFunctionContainer::operator +=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionPlus(content, in.content->copy()));
    return *this;
    }
RFunctionContainer& RFunctionContainer::operator -=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionMinus(content, in.content->copy()));
    return *this;
    }
RFunctionContainer& RFunctionContainer::operator *=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionMultiply(content, in.content->copy()));
    return *this;
    }
RFunctionContainer& RFunctionContainer::operator /=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionDivide(content, in.content->copy()));
    return *this;
    }

RFunctionContainer RFunctionContainer::operator +(const double& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionPlus(content->copy(),
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return tmp;
    }
RFunctionContainer RFunctionContainer::operator -(const double& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionMinus(content->copy(),
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return tmp;
    }
RFunctionContainer RFunctionContainer::operator *(const double& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionMultiply(content->copy(),
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return tmp;
    }
RFunctionContainer RFunctionContainer::operator /(const double& in)const
    {
    RFunctionContainer tmp;
    tmp.content = std::shared_ptr<IRFunction>(
            new RFunctionDivide(content->copy(),
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return tmp;
    }
RFunctionContainer& RFunctionContainer::operator +=(
        const double& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionPlus(content,
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return *this;
    }
RFunctionContainer& RFunctionContainer::operator -=(
        const double& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionMinus(content,
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return *this;
    }
RFunctionContainer& RFunctionContainer::operator *=(
        const double& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionMultiply(content,
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return *this;
    }
RFunctionContainer& RFunctionContainer::operator /=(
        const double& in)
    {
    content = std::shared_ptr<IRFunction>(
            new RFunctionDivide(content,
                    std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in))));
    return *this;
    }

const std::shared_ptr<IRFunction>& RFunctionContainer::GetContent()const
    {
    return content;
    }

std::string RFunctionContainer::to_string()const
    {
    return content->to_string();
    }
///RFunctionContainerTime
RFunctionContainerTime::RFunctionContainerTime()
    {
    content=std::shared_ptr<IRFunction>(new RFunctionTime);
    }
RFunctionContainerTime& RFunctionContainerTime::get_instance()
    {
    return RFunctionContainerTimeInstance;
    }

RFunctionContainerTime RFunctionContainerTime::RFunctionContainerTimeInstance;
/// RFunction classes
std::shared_ptr<IRFunction> RFunctionVoid::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionVoid);
    }

std::string RFunctionVoid::to_string(void) const
    {
    throw std::runtime_error("Trying to print void complex function");
    }

std::shared_ptr<IRFunction> RFunctionTime::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionTime);
    }

std::string RFunctionTime::to_string(void) const
    {
    return "t";
    }

RFunctionUnarMinus::RFunctionUnarMinus(const std::shared_ptr<IRFunction>& in) :
        argument(in)
    {
    }

std::shared_ptr<IRFunction> RFunctionUnarMinus::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionUnarMinus(argument));
    }

std::string RFunctionUnarMinus::to_string(void) const
    {
    return "-(" + argument->to_string() + ")";
    }

RFunctionPlus::RFunctionPlus(const std::shared_ptr<IRFunction>& in1,
        const std::shared_ptr<IRFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<IRFunction> RFunctionPlus::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionPlus(argument1, argument2));
    }

std::string RFunctionPlus::to_string(void) const
    {
    return "(" + argument1->to_string() + ")+(" + argument2->to_string() + ")";
    }

RFunctionMinus::RFunctionMinus(const std::shared_ptr<IRFunction>& in1,
        const std::shared_ptr<IRFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<IRFunction> RFunctionMinus::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionMinus(argument1, argument2));
    }

std::string RFunctionMinus::to_string(void) const
    {
    return "(" + argument1->to_string() + ")-(" + argument2->to_string() + ")";
    }

RFunctionMultiply::RFunctionMultiply(const std::shared_ptr<IRFunction>& in1,
        const std::shared_ptr<IRFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<IRFunction> RFunctionMultiply::copy(void) const
    {
    return std::shared_ptr<IRFunction>(
            new RFunctionMultiply(argument1, argument2));
    }

std::string RFunctionMultiply::to_string(void) const
    {
    return "(" + argument1->to_string() + ")*(" + argument2->to_string() + ")";
    }

RFunctionDivide::RFunctionDivide(const std::shared_ptr<IRFunction>& in1,
        const std::shared_ptr<IRFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<IRFunction> RFunctionDivide::copy(void) const
    {
    return std::shared_ptr<IRFunction>(
            new RFunctionDivide(argument1, argument2));
    }

std::string RFunctionDivide::to_string(void) const
    {
    return "(" + argument1->to_string() + ")/(" + argument2->to_string() + ")";
    }

RFunctionFunction::RFunctionFunction(const std::string& n,const std::shared_ptr<IRFunction>& in):
        argument(in),name(n)
    {
    }

std::shared_ptr<IRFunction> RFunctionFunction::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionFunction(name,argument));
    }

std::string RFunctionFunction::to_string(void) const
    {
    return name+"("+argument->to_string()+")";
    }

RFunctionFunction2::RFunctionFunction2(const std::string& n,
        const std::shared_ptr<IRFunction>& in1,const std::shared_ptr<IRFunction>& in2):
        argument1(in1),argument2(in2),name(n)
    {
    }

std::shared_ptr<IRFunction> RFunctionFunction2::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionFunction2(name,argument1,argument2));
    }

std::string RFunctionFunction2::to_string(void) const
    {
    return name+"("+argument1->to_string()+","+argument2->to_string()+")";
    }

RFunctionParameter::RFunctionParameter(const std::string & name):parameter_name(name)
    {
    }
std::shared_ptr<IRFunction> RFunctionParameter::copy(void) const
    {
    return std::shared_ptr<IRFunction>(new RFunctionParameter(parameter_name));
    }

std::string RFunctionParameter::to_string(void) const
    {
    return parameter_name;
    }

RFunctionContainer Abstr::operator+(const double& in,const RFunctionContainer& in1)
    {
    return in1+in;
    }
RFunctionContainer Abstr::operator-(const double& in,const RFunctionContainer& in1)
    {
    return -in1+in;
    }
RFunctionContainer Abstr::operator*(const double& in,const RFunctionContainer& in1)
    {
    return in1*in;
    }
RFunctionContainer Abstr::operator/(const double& in,const RFunctionContainer& in1)
    {
    RFunctionContainer tmp;
    tmp/=in1;
    return tmp*in;
    }
RFunctionContainer Abstr::sqrt(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "sqrt",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::sin(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "sin",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::cos(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "cos",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::tan(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "tan",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::ctan(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "ctan",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::exp(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "exp",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::sinh(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "sinh",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::cosh(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "cosh",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::tanh(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "tanh",in.GetContent()->copy())));
    }
RFunctionContainer Abstr::atan(const RFunctionContainer& in)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction(
            "atan",in.GetContent()->copy())));
    }

RFunctionContainer Abstr::pow(const RFunctionContainer& in,const RFunctionContainer& in1)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction2(
            "pow",in.GetContent()->copy(),in1.GetContent()->copy())));
    }
RFunctionContainer Abstr::pow(const RFunctionContainer& in,const double& in1)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction2(
                "pow",in.GetContent()->copy(),std::shared_ptr<IRFunction>(
                        new RFunctionNumber<double>(in1)))));
    }
RFunctionContainer Abstr::pow(const double& in,const RFunctionContainer& in1)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction2(
                "pow",std::shared_ptr<IRFunction>(new RFunctionNumber<double>(in)),
                in1.GetContent()->copy())));
    }

RFunctionContainer Abstr::wiener(const int& in,const RFunctionContainer& in1)
    {
    return RFunctionContainer(std::shared_ptr<IRFunction>(new RFunctionFunction2(
                "wiener",std::shared_ptr<IRFunction>(new RFunctionNumber<int>(in)),
                in1.GetContent()->copy())));
    }

RFunctionContainer Abstr::parameter(const std::string& in)
    {
    return RFunctionContainer(std::make_shared<RFunctionParameter>(in));
    }
