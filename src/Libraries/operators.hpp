#ifndef __Abstr_hpp_
#define __Abstr_hpp_
#include <list>
#include "const_operators.hpp"
#include "complex_function.hpp"

namespace Abstr
    {
class Operator
    {
private:
    struct Summand
    {
    CFunctionContainer coefficient;
    MatrixContainer matrix;
    };
    bool constant_constituent_exists;
    MatrixContainer constant_constituent;
    std::list<Summand> constituents;
public:
    Operator();
    ~Operator(){};
    Operator(const Operator&);
    Operator(const MatrixContainer&);
    Operator(const CFunctionContainer&);
    Operator(const RFunctionContainer&);
    Operator(const complex<double>&);
    Operator(const double&);

    Operator& operator=(const Operator&);
    Operator& operator=(const MatrixContainer&);
    Operator& operator=(const CFunctionContainer&);
    Operator& operator=(const RFunctionContainer&);
    Operator& operator=(const complex<double>&);
    Operator& operator=(const double&);

    Operator operator- (void)const;

    Operator operator+ (const Operator&)const;
    Operator operator+ (const MatrixContainer&)const;
    Operator operator+ (const CFunctionContainer&)const;
    Operator operator+ (const RFunctionContainer&)const;
    Operator operator+ (const complex<double>&)const;
    Operator operator+ (const double&)const;

    Operator operator- (const Operator&)const;
    Operator operator- (const MatrixContainer&)const;
    Operator operator- (const CFunctionContainer&)const;
    Operator operator- (const RFunctionContainer&)const;
    Operator operator- (const complex<double>&)const;
    Operator operator- (const double&)const;

    Operator operator* (const Operator&)const;
    Operator operator* (const MatrixContainer&)const;
    Operator operator* (const CFunctionContainer&)const;
    Operator operator* (const RFunctionContainer&)const;
    Operator operator* (const complex<double>&)const;
    Operator operator* (const double&)const;

    Operator operator/ (const CFunctionContainer&)const;
    Operator operator/ (const RFunctionContainer&)const;
    Operator operator/ (const complex<double>&)const;
    Operator operator/ (const double&)const;

    Operator& operator+= (const Operator&);
    Operator& operator+= (const MatrixContainer&);
    Operator& operator+= (const CFunctionContainer&);
    Operator& operator+= (const RFunctionContainer&);
    Operator& operator+= (const complex<double>&);
    Operator& operator+= (const double&);

    Operator& operator-= (const Operator&);
    Operator& operator-= (const MatrixContainer&);
    Operator& operator-= (const CFunctionContainer&);
    Operator& operator-= (const RFunctionContainer&);
    Operator& operator-= (const complex<double>&);
    Operator& operator-= (const double&);

    Operator& operator*= (const Operator&);
    Operator& operator*= (const MatrixContainer&);
    Operator& operator*= (const CFunctionContainer&);
    Operator& operator*= (const RFunctionContainer&);
    Operator& operator*= (const complex<double>&);
    Operator& operator*= (const double&);

    Operator& operator/= (const CFunctionContainer&);
    Operator& operator/= (const RFunctionContainer&);
    Operator& operator/= (const complex<double>&);
    Operator& operator/= (const double&);

    Operator conjuction(void) const;
    void save(const YAML::Node&,const std::string& operator_name);
    unsigned int GetNumberOfMatrices(void);
    };

    Operator operator+(const MatrixContainer&,const Operator&);
    Operator operator+(const CFunctionContainer&,const Operator&);
    Operator operator+(const RFunctionContainer&,const Operator&);
    Operator operator+(const complex<double>&,const Operator&);
    Operator operator+(const double&,const Operator&);

    Operator operator-(const MatrixContainer&,const Operator&);
    Operator operator-(const CFunctionContainer&,const Operator&);
    Operator operator-(const RFunctionContainer&,const Operator&);
    Operator operator-(const complex<double>&,const Operator&);
    Operator operator-(const double&,const Operator&);

    Operator operator*(const MatrixContainer&,const Operator&);
    Operator operator*(const CFunctionContainer&,const Operator&);
    Operator operator*(const RFunctionContainer&,const Operator&);
    Operator operator*(const complex<double>&,const Operator&);
    Operator operator*(const double&,const Operator&);

    Operator operator+(const MatrixContainer&,const CFunctionContainer&);
    Operator operator+(const CFunctionContainer&,const MatrixContainer&);
    Operator operator+(const MatrixContainer&,const RFunctionContainer&);
    Operator operator+(const RFunctionContainer&,const MatrixContainer&);

    Operator operator-(const MatrixContainer&,const CFunctionContainer&);
    Operator operator-(const CFunctionContainer&,const MatrixContainer&);
    Operator operator-(const MatrixContainer&,const RFunctionContainer&);
    Operator operator-(const RFunctionContainer&,const MatrixContainer&);

    Operator operator*(const MatrixContainer&,const CFunctionContainer&);
    Operator operator*(const CFunctionContainer&,const MatrixContainer&);
    Operator operator*(const MatrixContainer&,const RFunctionContainer&);
    Operator operator*(const RFunctionContainer&,const MatrixContainer&);

    Operator operator/(const MatrixContainer&,const CFunctionContainer&);
    Operator operator/(const MatrixContainer&,const RFunctionContainer&);

    Operator conj(const Operator&);
    }



#endif /*__Abstr_hpp_*/
