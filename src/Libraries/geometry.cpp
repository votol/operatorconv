#include <stdexcept>
#include "geometry.hpp"

using namespace Abstr;

bool Geometry::geometry_is_set=false;
std::vector<unsigned int> Geometry::dimensions_for_setting;

Geometry::Geometry(const std::vector<unsigned int>& in):dimensions(in)
    {
    total_dimension=0;
    for(auto it=dimensions.begin();it!=dimensions.end();++it)
        {
        total_dimension*=*it;
        }
    }

Geometry& Geometry::GetInstance(void)
    {
    if(!geometry_is_set)
        throw std::runtime_error("Trying to use Geometry before setting");
    static Geometry geometry_instanse(dimensions_for_setting);
    return geometry_instanse;
    }

const unsigned int & Geometry::GetDimension(const unsigned int& in)
    {
    if(in>=dimensions.size())
        throw std::invalid_argument("Trying to get dimension of non existing particle");
    return dimensions[in];
    }

const unsigned int& Geometry::GetTotalDimension(void)
    {
    return total_dimension;
    }

void Geometry::SetGeometry(const std::vector<unsigned int>& in)
    {
    if(geometry_is_set)
        throw std::runtime_error("Geometry already is set");
    geometry_is_set=true;
    dimensions_for_setting=in;
    }
void Geometry::SetGeometry(const YAML::Node& in)
    {
    if(geometry_is_set)
        throw std::runtime_error("Geometry already is set");
    geometry_is_set=true;
    dimensions_for_setting.resize(in.size());
    auto it1=in.begin();
    for(auto it=dimensions_for_setting.begin();it!=dimensions_for_setting.end();++it)
        {
        *it=(*it1)["dimension"].as<unsigned int>();
        it1++;
        }
    }



unsigned int Geometry::GetNumberOfParticles(void)
    {
    return dimensions.size();
    }
