#ifndef __geometry_hpp_
#define __geometry_hpp_

#include <vector>
#include"yaml-cpp/yaml.h"

namespace Abstr
    {
    class Geometry
        {
    private:
        std::vector<unsigned int> dimensions;
        unsigned int total_dimension;
        static bool geometry_is_set;
        static std::vector<unsigned int> dimensions_for_setting;
        Geometry(const std::vector<unsigned int>&);
    public:
        static Geometry& GetInstance(void);
        const unsigned int & GetDimension(const unsigned int&);
        const unsigned int & GetTotalDimension(void);
        static void SetGeometry(const std::vector<unsigned int>&);
        static void SetGeometry(const YAML::Node&);
        unsigned int GetNumberOfParticles(void);
        };
    };
#endif /*__geometry_hpp_*/
