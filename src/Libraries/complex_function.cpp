#include "complex_function.hpp"

using namespace Abstr;

CFunctionContainer::CFunctionContainer(void)
    {
    content = std::shared_ptr<ICFunction>(new CFunctionVoid);
    }

CFunctionContainer::CFunctionContainer(const CFunctionContainer& in)
    {
    content = in.content->copy();
    }

CFunctionContainer::CFunctionContainer(const RFunctionContainer& in)
    {
    content=std::shared_ptr<ICFunction>(new CFunctionRealFunction(in.GetContent()));
    }

CFunctionContainer::CFunctionContainer(const complex<double>& in)
    {
    content=std::shared_ptr<ICFunction>(new CFunctionNumber(in));
    }

CFunctionContainer::CFunctionContainer(const double& in)
    {
    content=std::shared_ptr<ICFunction>(new CFunctionNumber(in));
    }

CFunctionContainer::CFunctionContainer(const std::shared_ptr<ICFunction>& in):content(in)
    {

    }

CFunctionContainer& CFunctionContainer::operator =(const CFunctionContainer& in)
    {
    content = in.content->copy();
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator =(const RFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(new CFunctionRealFunction(in.GetContent()));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator =(const complex<double>& in)
    {
    content = std::shared_ptr<ICFunction>(new CFunctionNumber(in));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator =(const double& in)
    {
    content = std::shared_ptr<ICFunction>(new CFunctionNumber(in));
    return *this;
    }

CFunctionContainer CFunctionContainer::operator -()const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionUnarMinus(content->copy()));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator +(const CFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content->copy(), in.content->copy()));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator -(const CFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content->copy(), in.content->copy()));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator *(const CFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content->copy(), in.content->copy()));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator /(const CFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content->copy(), in.content->copy()));
    return tmp;
    }

CFunctionContainer& CFunctionContainer::operator +=(
        const CFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content, in.content->copy()));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator -=(
        const CFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content, in.content->copy()));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator *=(
        const CFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content, in.content->copy()));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator /=(
        const CFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content, in.content->copy()));
    return *this;
    }
CFunctionContainer CFunctionContainer::operator +(const complex<double>& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator -(const complex<double>& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator *(const complex<double>& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator /(const complex<double>& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer& CFunctionContainer::operator +=(
        const complex<double>& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator -=(
        const complex<double>& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator *=(
        const complex<double>& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator /=(
        const complex<double>& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }
CFunctionContainer CFunctionContainer::operator +(const double& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator -(const double& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator *(const double& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator /(const double& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return tmp;
    }
CFunctionContainer& CFunctionContainer::operator +=(
        const double& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator -=(
        const double& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator *=(
        const double& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator /=(
        const double& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content,
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in))));
    return *this;
    }

CFunctionContainer CFunctionContainer::operator +(const RFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator -(const RFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator *(const RFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return tmp;
    }
CFunctionContainer CFunctionContainer::operator /(const RFunctionContainer& in)const
    {
    CFunctionContainer tmp;
    tmp.content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content->copy(),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return tmp;
    }
CFunctionContainer& CFunctionContainer::operator +=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionPlus(content,
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator -=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMinus(content,
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator *=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionMultiply(content,
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return *this;
    }
CFunctionContainer& CFunctionContainer::operator /=(
        const RFunctionContainer& in)
    {
    content = std::shared_ptr<ICFunction>(
            new CFunctionDivide(content,
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(
                            in.GetContent()))));
    return *this;
    }

const std::shared_ptr<ICFunction>& CFunctionContainer::GetContent()const
    {
    return content;
    }
std::string CFunctionContainer::to_string()const
    {
    return content->to_string();
    }
/// CFunction classes
std::shared_ptr<ICFunction> CFunctionVoid::copy(void) const
    {
    return std::shared_ptr<ICFunction>(new CFunctionVoid);
    }

std::string CFunctionVoid::to_string(void) const
    {
    throw std::runtime_error("Trying to print void complex function");
    }

CFunctionUnarMinus::CFunctionUnarMinus(const std::shared_ptr<ICFunction>& in) :
        argument(in)
    {
    }

std::shared_ptr<ICFunction> CFunctionUnarMinus::copy(void) const
    {
    return std::shared_ptr<ICFunction>(new CFunctionUnarMinus(argument));
    }

std::string CFunctionUnarMinus::to_string(void) const
    {
    return "-(" + argument->to_string() + ")";
    }

CFunctionPlus::CFunctionPlus(const std::shared_ptr<ICFunction>& in1,
        const std::shared_ptr<ICFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<ICFunction> CFunctionPlus::copy(void) const
    {
    return std::shared_ptr<ICFunction>(new CFunctionPlus(argument1, argument2));
    }

std::string CFunctionPlus::to_string(void) const
    {
    return "(" + argument1->to_string() + ")+(" + argument2->to_string() + ")";
    }

CFunctionMinus::CFunctionMinus(const std::shared_ptr<ICFunction>& in1,
        const std::shared_ptr<ICFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<ICFunction> CFunctionMinus::copy(void) const
    {
    return std::shared_ptr<ICFunction>(new CFunctionMinus(argument1, argument2));
    }

std::string CFunctionMinus::to_string(void) const
    {
    return "(" + argument1->to_string() + ")-(" + argument2->to_string() + ")";
    }

CFunctionMultiply::CFunctionMultiply(const std::shared_ptr<ICFunction>& in1,
        const std::shared_ptr<ICFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<ICFunction> CFunctionMultiply::copy(void) const
    {
    return std::shared_ptr<ICFunction>(
            new CFunctionMultiply(argument1, argument2));
    }

std::string CFunctionMultiply::to_string(void) const
    {
    return "(" + argument1->to_string() + ")*(" + argument2->to_string() + ")";
    }

CFunctionDivide::CFunctionDivide(const std::shared_ptr<ICFunction>& in1,
        const std::shared_ptr<ICFunction>& in2) :
        argument1(in1), argument2(in2)
    {

    }

std::shared_ptr<ICFunction> CFunctionDivide::copy(void) const
    {
    return std::shared_ptr<ICFunction>(
            new CFunctionDivide(argument1, argument2));
    }

std::string CFunctionDivide::to_string(void) const
    {
    return "(" + argument1->to_string() + ")/(" + argument2->to_string() + ")";
    }

CFunctionNumber::CFunctionNumber(const complex<double>& in) :
        number(in)
    {
    }
CFunctionNumber::CFunctionNumber(const double& in) :
        number(in)
    {
    }
std::shared_ptr<ICFunction> CFunctionNumber::copy(void) const
    {
    return std::shared_ptr<ICFunction>(new CFunctionNumber(number));
    }
std::string CFunctionNumber::to_string(void) const
    {
    if (number.__im == 0.0)
        return std::to_string(number.__re);
    else if (number.__im == 1.0)
        {
        if(number.__re == 0.0)
            return "i";
        else
            return std::to_string(number.__re) +"+" +"i";
        }
    else
        return std::to_string(number.__re) +"+"+ std::to_string(number.__im) + "*i";
    }

CFunctionFunction::CFunctionFunction(const std::string& n,const std::shared_ptr<ICFunction>& in):
        argument(in),name(n)
    {
    }

std::shared_ptr<ICFunction> CFunctionFunction::copy(void) const
    {
    return std::shared_ptr<ICFunction>(new CFunctionFunction(name,argument));
    }

std::string CFunctionFunction::to_string(void) const
    {
    return name+"("+argument->to_string()+")";
    }

CFunctionRealFunction::CFunctionRealFunction(const std::shared_ptr<IRFunction>& in):
        argument(in)
    {
    }
std::shared_ptr<ICFunction> CFunctionRealFunction::copy(void) const
    {
    return std::shared_ptr<ICFunction>(new CFunctionRealFunction(argument));
    }
std::string CFunctionRealFunction::to_string(void) const
    {
    return argument->to_string();
    }

///externel operators
CFunctionContainer Abstr::operator+(const double& in,const CFunctionContainer& in1)
    {
    return in1+in;
    }
CFunctionContainer Abstr::operator-(const double& in,const CFunctionContainer& in1)
    {
    return -in1+in;
    }
CFunctionContainer Abstr::operator*(const double& in,const CFunctionContainer& in1)
    {
    return in1*in;
    }
CFunctionContainer Abstr::operator/(const double& in,const CFunctionContainer& in1)
    {
    CFunctionContainer tmp;
    tmp/=in1;
    return tmp*in;
    }
CFunctionContainer Abstr::operator+(const complex<double>& in,const CFunctionContainer& in1)
    {
    return in1+in;
    }
CFunctionContainer Abstr::operator-(const complex<double>& in,const CFunctionContainer& in1)
    {
    return -in1+in;
    }
CFunctionContainer Abstr::operator*(const complex<double>& in,const CFunctionContainer& in1)
    {
    return in1*in;
    }
CFunctionContainer Abstr::operator/(const complex<double>& in,const CFunctionContainer& in1)
    {
    CFunctionContainer tmp;
    tmp/=in1;
    return tmp*in;
    }

CFunctionContainer Abstr::operator+(const RFunctionContainer& in,const CFunctionContainer& in1)
    {
    return in1+in;
    }
CFunctionContainer Abstr::operator-(const RFunctionContainer& in,const CFunctionContainer& in1)
    {
    return -in1+in;
    }
CFunctionContainer Abstr::operator*(const RFunctionContainer& in,const CFunctionContainer& in1)
    {
    return in1*in;
    }
CFunctionContainer Abstr::operator/(const RFunctionContainer& in,const CFunctionContainer& in1)
    {
    CFunctionContainer tmp;
    tmp/=in1;
    return tmp*in;
    }

CFunctionContainer Abstr::operator+(const complex<double>& in,const RFunctionContainer& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionPlus(std::shared_ptr<ICFunction>(new CFunctionNumber(in)),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(in1.GetContent())))));
    return tmp;
    }
CFunctionContainer Abstr::operator-(const complex<double>& in,const RFunctionContainer& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionMinus(std::shared_ptr<ICFunction>(new CFunctionNumber(in)),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(in1.GetContent())))));
    return tmp;
    }
CFunctionContainer Abstr::operator*(const complex<double>& in,const RFunctionContainer& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionMultiply(std::shared_ptr<ICFunction>(new CFunctionNumber(in)),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(in1.GetContent())))));
    return tmp;
    }
CFunctionContainer Abstr::operator/(const complex<double>& in,const RFunctionContainer& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionDivide(std::shared_ptr<ICFunction>(new CFunctionNumber(in)),
                    std::shared_ptr<ICFunction>(new CFunctionRealFunction(in1.GetContent())))));
    return tmp;
    }

CFunctionContainer Abstr::operator+(const RFunctionContainer& in,const complex<double>& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionPlus(std::shared_ptr<ICFunction>(new
                    CFunctionRealFunction(in.GetContent())),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in1)))));
    return tmp;
    }
CFunctionContainer Abstr::operator-(const RFunctionContainer& in,const complex<double>& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionMinus(std::shared_ptr<ICFunction>(new
                    CFunctionRealFunction(in.GetContent())),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in1)))));
    return tmp;
    }
CFunctionContainer Abstr::operator*(const RFunctionContainer& in,const complex<double>& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionMultiply(std::shared_ptr<ICFunction>(new
                    CFunctionRealFunction(in.GetContent())),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in1)))));
    return tmp;
    }
CFunctionContainer Abstr::operator/(const RFunctionContainer& in,const complex<double>& in1)
    {
    CFunctionContainer tmp(std::shared_ptr<ICFunction>(new
            CFunctionDivide(std::shared_ptr<ICFunction>(new
                    CFunctionRealFunction(in.GetContent())),
                    std::shared_ptr<ICFunction>(new CFunctionNumber(in1)))));
    return tmp;
    }

CFunctionContainer Abstr::sin(const CFunctionContainer& in)
    {
    return CFunctionContainer(std::shared_ptr<ICFunction>(new CFunctionFunction(
            "sin",in.GetContent()->copy())));
    }
CFunctionContainer Abstr::cos(const CFunctionContainer& in)
    {
    return CFunctionContainer(std::shared_ptr<ICFunction>(new CFunctionFunction(
            "cos",in.GetContent()->copy())));
    }
CFunctionContainer Abstr::exp(const CFunctionContainer& in)
    {
    return CFunctionContainer(std::shared_ptr<ICFunction>(new CFunctionFunction(
            "exp",in.GetContent()->copy())));
    }
CFunctionContainer Abstr::conj(const CFunctionContainer& in)
    {
    return CFunctionContainer(std::shared_ptr<ICFunction>(new CFunctionFunction(
            "conj",in.GetContent()->copy())));
    }
