#ifndef __real_function_hpp_
#define __real_function_hpp_
#include <iostream>
#include <memory>
#include <string>


namespace Abstr
    {
    class IRFunction
        {
    public:
        IRFunction(){};
        virtual ~IRFunction(){};
        virtual std::shared_ptr<IRFunction> copy(void) const =0;
        virtual std::string to_string(void) const =0;
        };

    class RFunctionContainer
        {
    protected:
        std::shared_ptr<IRFunction> content;
    public:
        RFunctionContainer();
        ~RFunctionContainer(){};
        RFunctionContainer(const RFunctionContainer&);
        RFunctionContainer(const std::shared_ptr<IRFunction>&);
        RFunctionContainer& operator=(const RFunctionContainer&);
        RFunctionContainer& operator=(const double&);

        RFunctionContainer operator-()const;

        RFunctionContainer operator+(const RFunctionContainer&)const;
        RFunctionContainer operator-(const RFunctionContainer&)const;
        RFunctionContainer operator*(const RFunctionContainer&)const;
        RFunctionContainer operator/(const RFunctionContainer&)const;
        RFunctionContainer& operator+=(const RFunctionContainer&);
        RFunctionContainer& operator-=(const RFunctionContainer&);
        RFunctionContainer& operator*=(const RFunctionContainer&);
        RFunctionContainer& operator/=(const RFunctionContainer&);

        RFunctionContainer operator+(const double&)const;
        RFunctionContainer operator-(const double&)const;
        RFunctionContainer operator*(const double&)const;
        RFunctionContainer operator/(const double&)const;
        RFunctionContainer& operator+=(const double&);
        RFunctionContainer& operator-=(const double&);
        RFunctionContainer& operator*=(const double&);
        RFunctionContainer& operator/=(const double&);

        const std::shared_ptr<IRFunction>& GetContent()const;
        std::string to_string()const;
        };

    class RFunctionContainerTime:public RFunctionContainer
        {
    private:
        static RFunctionContainerTime RFunctionContainerTimeInstance;
        RFunctionContainerTime();

    public:
        static RFunctionContainerTime& get_instance();
        };


    class RFunctionVoid: public IRFunction
        {
    public:
        RFunctionVoid(){};
        ~RFunctionVoid(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };

    class RFunctionTime: public IRFunction
        {
    public:
        RFunctionTime(){};
        ~RFunctionTime(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };

    class RFunctionUnarMinus: public IRFunction
        {
    private:
        std::shared_ptr<IRFunction> argument;
    public:
        RFunctionUnarMinus(const std::shared_ptr<IRFunction>&);
        ~RFunctionUnarMinus(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };

    class RFunctionPlus: public IRFunction
        {
    private:
        std::shared_ptr<IRFunction> argument1;
        std::shared_ptr<IRFunction> argument2;
    public:
        RFunctionPlus(const std::shared_ptr<IRFunction>&,
                const std::shared_ptr<IRFunction>&);
        ~RFunctionPlus(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class RFunctionMinus: public IRFunction
        {
    private:
        std::shared_ptr<IRFunction> argument1;
        std::shared_ptr<IRFunction> argument2;
    public:
        RFunctionMinus(const std::shared_ptr<IRFunction>&,
                const std::shared_ptr<IRFunction>&);
        ~RFunctionMinus(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class RFunctionMultiply: public IRFunction
        {
    private:
        std::shared_ptr<IRFunction> argument1;
        std::shared_ptr<IRFunction> argument2;
    public:
        RFunctionMultiply(const std::shared_ptr<IRFunction>&,
                const std::shared_ptr<IRFunction>&);
        ~RFunctionMultiply(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class RFunctionDivide: public IRFunction
        {
    private:
        std::shared_ptr<IRFunction> argument1;
        std::shared_ptr<IRFunction> argument2;
    public:
        RFunctionDivide(const std::shared_ptr<IRFunction>&,
                const std::shared_ptr<IRFunction>&);
        ~RFunctionDivide(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    template<typename T>
    class RFunctionNumber: public IRFunction
        {
    private:
        T number;
    public:
        RFunctionNumber(const T& in):number(in)
            {
            }
        ~RFunctionNumber(){};
        std::shared_ptr<IRFunction> copy(void) const override
            {
            return std::shared_ptr<IRFunction>(new RFunctionNumber<T>(number));
            }
        std::string to_string(void) const override
            {
            return std::to_string(number);
            }
        };
    class RFunctionFunction: public IRFunction
        {
    private:
        std::shared_ptr<IRFunction> argument;
        std::string name;
    public:
        RFunctionFunction(const std::string&,const std::shared_ptr<IRFunction>&);
        ~RFunctionFunction(){};
        std::shared_ptr<IRFunction> copy(void) const override;
        std::string to_string(void) const override;
        };
    class RFunctionFunction2: public IRFunction
        {
        private:
            std::shared_ptr<IRFunction> argument1;
            std::shared_ptr<IRFunction> argument2;
            std::string name;
        public:
            RFunctionFunction2(const std::string&,const std::shared_ptr<IRFunction>&,const std::shared_ptr<IRFunction>&);
            ~RFunctionFunction2(){};
            std::shared_ptr<IRFunction> copy(void) const override;
            std::string to_string(void) const override;
        };
    class RFunctionParameter:public IRFunction
        {
        private:
            std::string parameter_name;
        public:
            RFunctionParameter(const std::string&);
            ~RFunctionParameter(){};
            std::shared_ptr<IRFunction> copy(void) const override;
            std::string to_string(void) const override;
        };
    #define t RFunctionContainerTime::get_instance()

    RFunctionContainer operator+(const double& in,const RFunctionContainer& in1);
    RFunctionContainer operator-(const double& in,const RFunctionContainer& in1);
    RFunctionContainer operator*(const double& in,const RFunctionContainer& in1);
    RFunctionContainer operator/(const double& in,const RFunctionContainer& in1);
    RFunctionContainer sqrt(const RFunctionContainer& in);
    RFunctionContainer sin(const RFunctionContainer& in);
    RFunctionContainer cos(const RFunctionContainer& in);
    RFunctionContainer tan(const RFunctionContainer& in);
    RFunctionContainer ctan(const RFunctionContainer& in);
    RFunctionContainer exp(const RFunctionContainer& in);
    RFunctionContainer sinh(const RFunctionContainer& in);
    RFunctionContainer cosh(const RFunctionContainer& in);
    RFunctionContainer tanh(const RFunctionContainer& in);
    RFunctionContainer atan(const RFunctionContainer& in);
    RFunctionContainer pow(const RFunctionContainer& in,const RFunctionContainer& in1);
    RFunctionContainer pow(const RFunctionContainer& in,const double& in1);
    RFunctionContainer pow(const double& in,const RFunctionContainer& in1);
    RFunctionContainer wiener(const int& in,const RFunctionContainer& in1);
    RFunctionContainer parameter(const std::string&);
    };
#endif /*__real_function_hpp_*/
