#ifndef __const_operators_hpp_
#define __const_operators_hpp_
#include <set>
#include <memory>
#include <functional>
#include <map>
#include "matrix.hpp"
#include "geometry.hpp"
#include "complex.h"
/* MatrixImpl should be defined and
 * next functions are needed:
 * MatrixImpl(const unsigned int& rows_num,const unsigned int& colums_num,const unsigned int& non_zero_elements)
 * void SetValue(MatrixImpl& in,unsigned int row,unsigned int col,complex<double> val);
 * MatrixImpl operator*(MatrixImpl& a,MatrixImpl& b);
 * MatrixImpl operator*(MatrixImpl& a,const complex<double> b);
 * MatrixImpl operator+(MatrixImpl& a,MatrixImpl& b);
 * MatrixImpl operator-(MatrixImpl& a,MatrixImpl& b);
 * MatrixImpl operator-(MatrixImpl& a);
 * MatrixImpl MatrixTensorMul(MatrixImpl& a,MatrixImpl& b);
 * MatrixImpl MatrixExtend(const unsigned int dim,MatrixImpl& a);
 * MatrixImpl MatrixExtend(MatrixImpl& a,const unsigned int dim);
 *  */

#ifndef i
#define i complex<double>(0.0,1.0)
#endif

namespace Abstr
    {
    enum class IntersectionType{left,right,both,mixed,non};

    struct IntersectionResult
        {
        IntersectionType type;
        std::vector<unsigned int> dimension;
        std::vector<std::set<unsigned int> > sig;
        };

    class BaseMatrix
        {
    protected:
        std::set<unsigned int> signature;
    public:
        BaseMatrix(){};
        virtual ~BaseMatrix(){};
        virtual std::shared_ptr<BaseMatrix> copy(void) const =0;
        virtual MatrixImpl calculate(void) const =0;
        virtual void lead(const std::set<unsigned int>&)=0;
        virtual void print(void)=0;
        virtual std::shared_ptr<BaseMatrix> conjugation(void)=0;
        std::set<unsigned int>& GetSignature(void);

        };

    class MatrixContainer
        {
    protected:
        std::shared_ptr<BaseMatrix> child;
    public:
        MatrixContainer();
        virtual ~MatrixContainer(){};
        MatrixContainer(const MatrixContainer&);
        MatrixContainer(const std::shared_ptr<BaseMatrix>&);
        MatrixContainer& operator= (const MatrixContainer&);
        MatrixContainer operator- (void)const;
        MatrixContainer operator+ (const MatrixContainer&)const;
        MatrixContainer operator- (const MatrixContainer&)const;
        MatrixContainer operator* (const MatrixContainer&)const;

        MatrixContainer& operator+= (const MatrixContainer&);
        MatrixContainer& operator-= (const MatrixContainer&);
        MatrixContainer& operator*= (const MatrixContainer&);

        MatrixContainer operator+ (const complex<double>&)const;
        MatrixContainer operator- (const complex<double>&)const;
        MatrixContainer operator* (const complex<double>&)const;
        MatrixContainer operator/ (const complex<double>&)const;

        MatrixContainer& operator+= (const complex<double>&);
        MatrixContainer& operator-= (const complex<double>&);
        MatrixContainer& operator*= (const complex<double>&);
        MatrixContainer& operator/= (const complex<double>&);

        MatrixContainer operator+ (const double&)const;
        MatrixContainer operator- (const double&)const;
        MatrixContainer operator* (const double&)const;
        MatrixContainer operator/ (const double&)const;

        MatrixContainer& operator+= (const double&);
        MatrixContainer& operator-= (const double&);
        MatrixContainer& operator*= (const double&);
        MatrixContainer& operator/= (const double&);
        MatrixImpl calculate(void);
        void print(void);
        MatrixContainer conjugation(void) const;
        };


    MatrixContainer operator+(const complex<double>&,const MatrixContainer&);
    MatrixContainer operator-(const complex<double>&,const MatrixContainer&);
    MatrixContainer operator*(const complex<double>&,const MatrixContainer&);

    MatrixContainer operator+(const double&,const MatrixContainer&);
    MatrixContainer operator-(const double&,const MatrixContainer&);
    MatrixContainer operator*(const double&,const MatrixContainer&);

    MatrixContainer conj(const MatrixContainer&);

    class VoidMatrix:public BaseMatrix
        {
    public:
        VoidMatrix(){};
        ~VoidMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };
    class UnarMinusMatrix:public BaseMatrix
        {
        std::shared_ptr<BaseMatrix> arg;
    public:
        UnarMinusMatrix(const std::shared_ptr<BaseMatrix>&);
        ~UnarMinusMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };
    class PlusMatrix:public BaseMatrix
        {
        std::shared_ptr<BaseMatrix> first;
        std::shared_ptr<BaseMatrix> second;
    public:
        PlusMatrix(const std::shared_ptr<BaseMatrix>&,const std::shared_ptr<BaseMatrix>&);
        ~PlusMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };
    class MinusMatrix:public BaseMatrix
        {
        std::shared_ptr<BaseMatrix> first;
        std::shared_ptr<BaseMatrix> second;
    public:
        MinusMatrix(const std::shared_ptr<BaseMatrix>&,const std::shared_ptr<BaseMatrix>&);
        ~MinusMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };
    class MultiplyMatrix:public BaseMatrix
        {
        std::shared_ptr<BaseMatrix> first;
        std::shared_ptr<BaseMatrix> second;
    public:
        MultiplyMatrix(const std::shared_ptr<BaseMatrix>&,const std::shared_ptr<BaseMatrix>&);
        ~MultiplyMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };
    class MultiplyTensMatrix:public BaseMatrix
        {
        unsigned int dimension(const std::set<unsigned int>&);

        std::shared_ptr<BaseMatrix> first;
        std::shared_ptr<BaseMatrix> second;
    public:
        MultiplyTensMatrix(const std::shared_ptr<BaseMatrix>&,const std::shared_ptr<BaseMatrix>&);
        ~MultiplyTensMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };

    class MultiplyNumberMatrix:public BaseMatrix
        {
        std::shared_ptr<BaseMatrix> first;
        complex<double> number;
    public:
        MultiplyNumberMatrix(const std::shared_ptr<BaseMatrix>&,const complex<double>&);
        ~MultiplyNumberMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };
    class DiagonalMatrix:public BaseMatrix
        {
        complex<double> number;
    public:
        DiagonalMatrix(const complex<double>&);
        DiagonalMatrix(const complex<double>&,const std::set<unsigned int>&);
        ~DiagonalMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };

    class TensorLeadMatrix:public BaseMatrix
        {
    public:
        enum class options{left,right};
    private:
        struct OptionsPair
            {
        IntersectionType first;
        IntersectionType second;
        OptionsPair(const IntersectionType&,const IntersectionType&);
        OptionsPair(const OptionsPair&);
        OptionsPair& operator= (const OptionsPair&);
            };
        struct OptionsPairCompare
            {
            bool operator()(const OptionsPair&,const OptionsPair&);
            };

        void ll(const IntersectionResult&,const IntersectionResult&);
        void lb(const IntersectionResult&,const IntersectionResult&);
        void rr(const IntersectionResult&,const IntersectionResult&);
        void rb(const IntersectionResult&,const IntersectionResult&);
        void bb(const IntersectionResult&,const IntersectionResult&);
        void ml(const IntersectionResult&,const IntersectionResult&);
        void mr(const IntersectionResult&,const IntersectionResult&);
        void mb(const IntersectionResult&,const IntersectionResult&);
        void mm(const IntersectionResult&,const IntersectionResult&);

        using LeadFunction=std::function<void(const IntersectionResult&,const IntersectionResult&)>;
        std::map<OptionsPair,LeadFunction,OptionsPairCompare> lead_functions;
        std::shared_ptr<BaseMatrix> arg;
        unsigned int dimension;
        options opt;
    public:
        TensorLeadMatrix(const std::shared_ptr<BaseMatrix>&,unsigned int dim,const options& o,
                const std::set<unsigned int> sig);
        ~TensorLeadMatrix(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };

    class Matrix_a:public BaseMatrix
        {
        unsigned int particle_number;
    public:
        Matrix_a(unsigned int p_num);
        ~Matrix_a(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };

    class Matrix_ak:public BaseMatrix
        {
        unsigned int particle_number;
    public:
        Matrix_ak(unsigned int p_num);
        ~Matrix_ak(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };
    class Matrix_sig:public BaseMatrix
        {
        unsigned int particle_number;
        unsigned int f;
        unsigned int t;
    public:
        Matrix_sig(unsigned int p_num,unsigned int from,unsigned int to);
        ~Matrix_sig(){};
        std::shared_ptr<BaseMatrix> copy(void) const override;
        MatrixImpl calculate(void) const override;
        void lead(const std::set<unsigned int>&) override;
        void print(void) override;
        std::shared_ptr<BaseMatrix> conjugation(void) override;
        };

    class Particle
        {
        unsigned int number;
    public:
        Particle(const unsigned int&);
        ~Particle(){};
        MatrixContainer a();
        MatrixContainer ak();
        MatrixContainer sig(const unsigned int& from,const unsigned int to);
        };

    class ParticlesContainer
        {
        ParticlesContainer(){};
    public:
        ~ParticlesContainer(){};
        Particle operator()(const unsigned int&);
        static ParticlesContainer& GetInstance();
        };

    #define P(x) ParticlesContainer::GetInstance()(x)
    };
#endif /*__const_operators_hpp_*/
