cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)

project(OperatorsAbstractLibrary)
set (SOURCES real_function.cpp
             complex_function.cpp
             matrix.cpp
             geometry.cpp
             const_operators.cpp
             operators.cpp
             )
set (HEADERS 
	real_function.hpp
    complex_function.hpp
    matrix.hpp
    geometry.hpp
    const_operators.hpp
    operators.hpp
    )
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

include_directories (.)
include_directories (../types)


add_library(${PROJECT_NAME} ${SOURCES})
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 11)
add_subdirectory(../types ${CMAKE_CURRENT_BINARY_DIR}/types)

target_link_libraries (${PROJECT_NAME} sparse)
                                  
