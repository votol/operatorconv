#include <sys/stat.h>
#include <fstream>
#include <stdexcept>
#include "operators.hpp"


using namespace Abstr;

inline bool file_exists (const std::string& name)
    {
    struct stat buffer;
    return (stat (name.c_str(), &buffer) == 0);
    }


Operator::Operator()
    {
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
    constant_constituent_exists=false;
    }


Operator::Operator(const Operator& in)
    {
    constant_constituent=in.constant_constituent;
    constituents=in.constituents;
    constant_constituent_exists=in.constant_constituent_exists;
    }

Operator::Operator(const MatrixContainer& in)
    {
    constant_constituent=in;
    constant_constituent_exists=true;
    }

Operator::Operator(const CFunctionContainer& in)
    {
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
    constant_constituent_exists=false;
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    }

Operator::Operator(const RFunctionContainer& in)
    {
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
    constant_constituent_exists=false;
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    }

Operator::Operator(const complex<double>& in)
    {
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
    constant_constituent_exists=true;
    }

Operator::Operator(const double& in)
    {
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
    constant_constituent_exists=true;
    }

Operator& Operator::operator=(const Operator& in)
    {
    constant_constituent=in.constant_constituent;
    constituents=in.constituents;
    constant_constituent_exists=in.constant_constituent_exists;
    return *this;
    }
Operator& Operator::operator=(const MatrixContainer& in)
    {
    constituents.clear();
    constant_constituent=in;
    constant_constituent_exists=true;
    return *this;
    }
Operator& Operator::operator=(const CFunctionContainer& in)
    {
    constituents.clear();
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
    constant_constituent_exists=false;
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    return *this;
    }
Operator& Operator::operator=(const RFunctionContainer& in)
    {
    constituents.clear();
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
    constant_constituent_exists=false;
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    return *this;
    }
Operator& Operator::operator=(const complex<double>& in)
    {
    constituents.clear();
    constant_constituent_exists=true;
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
    return *this;
    }
Operator& Operator::operator=(const double& in)
    {
    constituents.clear();
    constant_constituent_exists=true;
    constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
    return *this;
    }

Operator Operator::operator- (void)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent=-tmp.constant_constituent;
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->matrix=-it->matrix;
    return tmp;
    }

Operator Operator::operator+ (const Operator& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists && in.constant_constituent_exists)
        {
        tmp.constant_constituent+=in.constant_constituent;
        tmp.constant_constituent_exists=true;
        }
    else if(in.constant_constituent_exists)
        {
        tmp.constant_constituent=in.constant_constituent;
        tmp.constant_constituent_exists=true;
        }
    for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
        {
        tmp.constituents.push_back(Summand());
        tmp.constituents.back().coefficient=it->coefficient;
        tmp.constituents.back().matrix=it->matrix;
        }
    return tmp;
    }
Operator Operator::operator+ (const MatrixContainer& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent+=in;
    else
        {
        tmp.constant_constituent=in;
        tmp.constant_constituent_exists=true;
        }
    return tmp;
    }
Operator Operator::operator+ (const CFunctionContainer& in)const
    {
    Operator tmp(*this);
    tmp.constituents.push_back(Summand());
    tmp.constituents.back().coefficient=in;
    tmp.constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    return tmp;
    }
Operator Operator::operator+ (const RFunctionContainer& in)const
    {
    Operator tmp(*this);
    tmp.constituents.push_back(Summand());
    tmp.constituents.back().coefficient=in;
    tmp.constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    return tmp;
    }
Operator Operator::operator+ (const complex<double>& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent+=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
    else
        {
        tmp.constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
        tmp.constant_constituent_exists=true;
        }
    return tmp;
    }
Operator Operator::operator+ (const double& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent+=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
    else
        {
        tmp.constant_constituent-=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
        tmp.constant_constituent_exists=true;
        }
    return tmp;
    }

Operator Operator::operator- (const Operator& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists && in.constant_constituent_exists)
        {
        tmp.constant_constituent-=in.constant_constituent;
        }
    else if(in.constant_constituent_exists)
        {
        tmp.constant_constituent=-in.constant_constituent;
        tmp.constant_constituent_exists=true;
        }
    for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
        {
        tmp.constituents.push_back(Summand());
        tmp.constituents.back().coefficient=it->coefficient;
        tmp.constituents.back().matrix=-it->matrix;
        }
    return tmp;
    }
Operator Operator::operator- (const MatrixContainer& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent-=in;
    else
        {
        tmp.constant_constituent=-in;
        tmp.constant_constituent_exists=true;
        }
    return tmp;
    }
Operator Operator::operator- (const CFunctionContainer& in)const
    {
    Operator tmp(*this);
    tmp.constituents.push_back(Summand());
    tmp.constituents.back().coefficient=in;
    tmp.constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(-1.0))));
    return tmp;
    }
Operator Operator::operator- (const RFunctionContainer& in)const
    {
    Operator tmp(*this);
    tmp.constituents.push_back(Summand());
    tmp.constituents.back().coefficient=in;
    tmp.constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(-1.0))));
    return tmp;
    }
Operator Operator::operator- (const complex<double>& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent-=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
    else
        {
        tmp.constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(-in)));
        tmp.constant_constituent_exists=true;
        }
    return tmp;
    }
Operator Operator::operator- (const double& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent-=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
    else
        {
        tmp.constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(-in))));
        tmp.constant_constituent_exists=true;
        }
    return tmp;
    }

Operator Operator::operator* (const Operator& in)const
    {
    Operator tmp;
    if(constant_constituent_exists && in.constant_constituent_exists)
        {
        tmp.constant_constituent=constant_constituent*in.constant_constituent;
        tmp.constant_constituent_exists=true;
        }
    if(in.constant_constituent_exists)
        {
        for(auto it=constituents.begin();it!=constituents.end();++it)
            {
            tmp.constituents.push_back(Summand());
            tmp.constituents.back().coefficient=it->coefficient;
            tmp.constituents.back().matrix=it->matrix*in.constant_constituent;
            }
        }

    if(constant_constituent_exists)
        {
        for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
            {
            tmp.constituents.push_back(Summand());
            tmp.constituents.back().coefficient=it->coefficient;
            tmp.constituents.back().matrix=it->matrix*constant_constituent;
            }
        }
    for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
        {
        for(auto it1=constituents.begin();it1!=constituents.end();++it1)
            {
            tmp.constituents.push_back(Summand());
            tmp.constituents.back().coefficient=it->coefficient*it1->coefficient;
            tmp.constituents.back().matrix=it->matrix*it1->matrix;
            }
        }
    return tmp;
    }
Operator Operator::operator* (const MatrixContainer& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent*=in;
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->matrix*=in;
    return tmp;
    }
Operator Operator::operator* (const CFunctionContainer& in)const
    {
    Operator tmp(*this);
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->coefficient*=in;
    if(constant_constituent_exists)
        {
        tmp.constituents.push_back(Summand());
        tmp.constituents.back().coefficient=in;
        tmp.constituents.back().matrix=constant_constituent;
        tmp.constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        tmp.constant_constituent_exists=false;
        }
    return tmp;
    }
Operator Operator::operator* (const RFunctionContainer& in)const
    {
    Operator tmp(*this);
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->coefficient*=in;
    if(constant_constituent_exists)
        {
        tmp.constituents.push_back(Summand());
        tmp.constituents.back().coefficient=in;
        tmp.constituents.back().matrix=constant_constituent;
        tmp.constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        tmp.constant_constituent_exists=false;
        }
    return tmp;
    }
Operator Operator::operator* (const complex<double>& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent*=in;
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->matrix*=in;
    return tmp;
    }
Operator Operator::operator* (const double& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent*=in;
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->matrix*=in;
    return tmp;
    }

Operator Operator::operator/ (const CFunctionContainer& in)const
    {
    Operator tmp(*this);
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->coefficient/=in;
    if(constant_constituent_exists)
        {
        tmp.constituents.push_back(Summand());
        tmp.constituents.back().coefficient=1.0/in;
        tmp.constituents.back().matrix=constant_constituent;
        tmp.constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        tmp.constant_constituent_exists=false;
        }
    return tmp;
    }
Operator Operator::operator/ (const RFunctionContainer& in)const
    {
    Operator tmp(*this);
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->coefficient/=in;
    if(constant_constituent_exists)
        {
        tmp.constituents.push_back(Summand());
        tmp.constituents.back().coefficient=1.0/in;
        tmp.constituents.back().matrix=constant_constituent;
        tmp.constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        tmp.constant_constituent_exists=false;
        }
    return tmp;
    }
Operator Operator::operator/ (const complex<double>& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent/=in;
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->matrix/=in;
    return tmp;
    }
Operator Operator::operator/ (const double& in)const
    {
    Operator tmp(*this);
    if(constant_constituent_exists)
        tmp.constant_constituent/=in;
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        it->matrix/=in;
    return tmp;
    }

///operators x=

Operator& Operator::operator+= (const Operator& in)
    {
    if(constant_constituent_exists && in.constant_constituent_exists)
        constant_constituent+=in.constant_constituent;
    else if(in.constant_constituent_exists)
        {
        constant_constituent=in.constant_constituent;
        constant_constituent_exists=true;
        }
    for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
        {
        constituents.push_back(Summand());
        constituents.back().coefficient=it->coefficient;
        constituents.back().matrix=it->matrix;
        }
    return *this;
    }
Operator& Operator::operator+= (const MatrixContainer& in)
    {
    if(constant_constituent_exists)
        constant_constituent+=in;
    else
        {
        constant_constituent=in;
        constant_constituent_exists=true;
        }
    return *this;
    }
Operator& Operator::operator+= (const CFunctionContainer& in)
    {
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    return *this;
    }
Operator& Operator::operator+= (const RFunctionContainer& in)
    {
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(1.0))));
    return *this;
    }
Operator& Operator::operator+= (const complex<double>& in)
    {
    if(constant_constituent_exists)
        constant_constituent+=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
    else
        {
        constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
        constant_constituent_exists=true;
        }
    return *this;
    }
Operator& Operator::operator+= (const double& in)
    {
    if(constant_constituent_exists)
        constant_constituent+=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
    else
        {
        constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
        constant_constituent_exists=true;
        }
    return *this;
    }

Operator& Operator::operator-= (const Operator& in)
    {
    if(constant_constituent_exists && in.constant_constituent_exists)
        constant_constituent-=in.constant_constituent;
    else if(in.constant_constituent_exists)
        {
        constant_constituent=-in.constant_constituent;
        constant_constituent_exists=true;
        }

    for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
        {
        constituents.push_back(Summand());
        constituents.back().coefficient=it->coefficient;
        constituents.back().matrix=-it->matrix;
        }
    return *this;
    }
Operator& Operator::operator-= (const MatrixContainer& in)
    {
    if(constant_constituent_exists)
        constant_constituent-=in;
    else
        {
        constant_constituent=-in;
        constant_constituent_exists=true;
        }
    return *this;
    }
Operator& Operator::operator-= (const CFunctionContainer& in)
    {
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(-1.0))));
    return *this;
    }
Operator& Operator::operator-= (const RFunctionContainer& in)
    {
    constituents.push_back(Summand());
    constituents.back().coefficient=in;
    constituents.back().matrix=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(-1.0))));
    return *this;
    }
Operator& Operator::operator-= (const complex<double>& in)
    {
    if(constant_constituent_exists)
        constant_constituent-=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
    else
        {
        constant_constituent=-MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(in)));
        constant_constituent_exists=true;
        }
    return *this;
    }
Operator& Operator::operator-= (const double& in)
    {
    if(constant_constituent_exists)
        constant_constituent-=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
    else
        {
        constant_constituent=-MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(in))));
        constant_constituent_exists=true;
        }
    return *this;
    }

Operator& Operator::operator*= (const Operator& in)
    {
    Operator tmp(*this);
    if(constant_constituent_exists && in.constant_constituent_exists)
        constant_constituent=tmp.constant_constituent*in.constant_constituent;
    else if(constant_constituent_exists)
        {
        constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        constant_constituent_exists=false;
        }
    constituents.clear();
    if(in.constant_constituent_exists)
        {
        for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
            {
            constituents.push_back(Summand());
            constituents.back().coefficient=it->coefficient;
            constituents.back().matrix=it->matrix*in.constant_constituent;
            }
        }
    if(tmp.constant_constituent_exists)
        {
        for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
            {
            constituents.push_back(Summand());
            constituents.back().coefficient=it->coefficient;
            constituents.back().matrix=it->matrix*tmp.constant_constituent;
            }
        }
    for(auto it=in.constituents.begin();it!=in.constituents.end();++it)
        {
        for(auto it1=tmp.constituents.begin();it1!=tmp.constituents.end();++it1)
            {
            constituents.push_back(Summand());
            constituents.back().coefficient=it->coefficient*it1->coefficient;
            constituents.back().matrix=it->matrix*it1->matrix;
            }
        }
    return *this;
    }
Operator& Operator::operator*= (const MatrixContainer& in)
    {
    if(constant_constituent_exists)
        constant_constituent*=in;
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->matrix*=in;
    return *this;
    }
Operator& Operator::operator*= (const CFunctionContainer& in)
    {
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->coefficient*=in;
    if(constant_constituent_exists)
        {
        constituents.push_back(Summand());
        constituents.back().coefficient=in;
        constituents.back().matrix=constant_constituent;
        constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        constant_constituent_exists=false;
        }
    return *this;
    }
Operator& Operator::operator*= (const RFunctionContainer& in)
    {
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->coefficient*=in;
    if(constant_constituent_exists)
        {
        constituents.push_back(Summand());
        constituents.back().coefficient=in;
        constituents.back().matrix=constant_constituent;
        constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        constant_constituent_exists=false;
        }
    return *this;
    }
Operator& Operator::operator*= (const complex<double>& in)
    {
    if(constant_constituent_exists)
        constant_constituent*=in;
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->matrix*=in;
    return *this;
    }
Operator& Operator::operator*= (const double& in)
    {
    if(constant_constituent_exists)
        constant_constituent*=in;
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->matrix*=in;
    return *this;
    }

Operator& Operator::operator/= (const CFunctionContainer& in)
    {
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->coefficient/=in;
    if(constant_constituent_exists)
        {
        constituents.push_back(Summand());
        constituents.back().coefficient=1.0/in;
        constituents.back().matrix=constant_constituent;
        constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        constant_constituent_exists=false;
        }
    return *this;
    }
Operator& Operator::operator/= (const RFunctionContainer& in)
    {
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->coefficient/=in;
    if(constant_constituent_exists)
        {
        constituents.push_back(Summand());
        constituents.back().coefficient=1.0/in;
        constituents.back().matrix=constant_constituent;
        constant_constituent=MatrixContainer(std::shared_ptr<BaseMatrix>(new DiagonalMatrix(complex<double>(0.0))));
        constant_constituent_exists=false;
        }
    return *this;
    }
Operator& Operator::operator/= (const complex<double>& in)
    {
    if(constant_constituent_exists)
        constant_constituent/=in;
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->matrix/=in;
    return *this;
    }
Operator& Operator::operator/= (const double& in)
    {
    if(constant_constituent_exists)
        constant_constituent/=in;
    for(auto it=constituents.begin();it!=constituents.end();++it)
        it->matrix/=in;
    return *this;
    }
Operator Operator::conjuction(void) const
    {
    Operator tmp(*this);
    if(tmp.constant_constituent_exists)
        tmp.constant_constituent=conj(tmp.constant_constituent);
    for(auto it=tmp.constituents.begin();it!=tmp.constituents.end();++it)
        {
        it->coefficient=conj(it->coefficient);
        it->matrix=conj(it->matrix);
        }
    return tmp;
    }


void Operator::save(const YAML::Node& config,const std::string& operator_name)
    {
    std::string output_yaml_file=config["paths"]["output_directory"].as<std::string>()+
            config["output"]["name"].as<std::string>()+".yml";
    std::string output_bin_file=config["paths"]["output_directory"].as<std::string>()+
                config["output"]["name"].as<std::string>()+".bin";
    YAML::Node output_yaml = YAML::LoadFile(output_yaml_file);
    if(output_yaml[operator_name])
        {
        throw std::runtime_error("Saving operator impossible: given name already exists");
        }
    unsigned int matrix_number=0;
    for(auto it=output_yaml.begin();it!=output_yaml.end();++it)
        {
        for(auto it1=it->second.begin();it1!=it->second.end();++it1)
            matrix_number++;
        }
    std::ofstream output_bin_file_stream(output_bin_file,std::ofstream::out | std::ofstream::app | std::ofstream::binary);
    if(constant_constituent_exists)
        {
        YAML::Node tmp;
        tmp["id"]=matrix_number;
        tmp["coefficient"]="1.0";
        output_yaml[operator_name].push_back(tmp);
        auto matr=constant_constituent.calculate();
        output_bin_file_stream<<matr;
        matrix_number++;
        }
    for(auto it=constituents.begin();it!=constituents.end();++it)
        {
        YAML::Node tmp;
        tmp["id"]=matrix_number;
        tmp["coefficient"]=it->coefficient.to_string();
        output_yaml[operator_name].push_back(tmp);
        auto matr=it->matrix.calculate();
        output_bin_file_stream<<matr;
        matrix_number++;
        }
    output_bin_file_stream.close();
    std::ofstream output_yaml_file_stream(output_yaml_file,std::ofstream::out | std::ofstream::trunc);
    output_yaml_file_stream << output_yaml;
    output_yaml_file_stream.close();
    }

unsigned int Operator::GetNumberOfMatrices(void)
	{
	if(constant_constituent_exists)
		return constituents.size() + 1;
	else
		return constituents.size();
	}

Operator Abstr::operator+(const MatrixContainer& a,const Operator& b)
    {
    return b+a;
    }
Operator Abstr::operator+(const CFunctionContainer& a,const Operator& b)
    {
    return b+a;
    }
Operator Abstr::operator+(const RFunctionContainer& a,const Operator& b)
    {
    return b+a;
    }
Operator Abstr::operator+(const complex<double>& a,const Operator& b)
    {
    return b+a;
    }
Operator Abstr::operator+(const double& a,const Operator& b)
    {
    return b+a;
    }
Operator Abstr::operator-(const MatrixContainer& a,const Operator& b)
    {
    return -b+a;
    }
Operator Abstr::operator-(const CFunctionContainer& a,const Operator& b)
    {
    return -b+a;
    }
Operator Abstr::operator-(const RFunctionContainer& a,const Operator& b)
    {
    return -b+a;
    }
Operator Abstr::operator-(const complex<double>& a,const Operator& b)
    {
    return -b+a;
    }
Operator Abstr::operator-(const double& a,const Operator& b)
    {
    return -b+a;
    }
Operator Abstr::operator*(const MatrixContainer& a,const Operator& b)
    {
    return b*a;
    }
Operator Abstr::operator*(const CFunctionContainer& a,const Operator& b)
    {
    return b*a;
    }
Operator Abstr::operator*(const RFunctionContainer& a,const Operator& b)
    {
    return b*a;
    }
Operator Abstr::operator*(const complex<double>& a,const Operator& b)
    {
    return b*a;
    }
Operator Abstr::operator*(const double& a,const Operator& b)
    {
    return b*a;
    }

Operator Abstr::operator+(const MatrixContainer& a,const CFunctionContainer& b)
    {
    Operator res(a);
    return res+b;
    }
Operator Abstr::operator+(const CFunctionContainer& a,const MatrixContainer& b)
    {
    Operator res(b);
    return res+a;
    }
Operator Abstr::operator+(const MatrixContainer& a,const RFunctionContainer& b)
    {
    Operator res(a);
    return res+b;
    }
Operator Abstr::operator+(const RFunctionContainer& a,const MatrixContainer& b)
    {
    Operator res(b);
    return res+a;
    }

Operator Abstr::operator-(const MatrixContainer& a,const CFunctionContainer& b)
    {
    Operator res(a);
    return res-b;
    }
Operator Abstr::operator-(const CFunctionContainer& a,const MatrixContainer& b)
    {
    Operator res(b);
    return -res+a;
    }
Operator Abstr::operator-(const MatrixContainer& a,const RFunctionContainer& b)
    {
    Operator res(a);
    return res-b;
    }
Operator Abstr::operator-(const RFunctionContainer& a,const MatrixContainer& b)
    {
    Operator res(b);
    return -res+a;
    }

Operator Abstr::operator*(const MatrixContainer& a,const CFunctionContainer& b)
    {
    Operator res(a);
    return res*b;
    }
Operator Abstr::operator*(const CFunctionContainer& a,const MatrixContainer& b)
    {
    Operator res(b);
    return res*a;
    }
Operator Abstr::operator*(const MatrixContainer& a,const RFunctionContainer& b)
    {
    Operator res(a);
    return res*b;
    }
Operator Abstr::operator*(const RFunctionContainer& a,const MatrixContainer& b)
    {
    Operator res(b);
    return res*a;
    }
Operator Abstr::operator/(const MatrixContainer& a,const CFunctionContainer& b)
    {
    Operator res(a);
    return res/b;
    }
Operator Abstr::operator/(const MatrixContainer& a,const RFunctionContainer& b)
    {
    Operator res(a);
    return res/b;
    }

Operator Abstr::conj(const Operator& in)
    {
    return in.conjuction();
    }
