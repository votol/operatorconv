#ifndef __matrix_hpp_
#define __matrix_hpp_
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <ostream>
#include "complex.h"

namespace Abstr
    {
    using MatrixImpl=boost::numeric::ublas::compressed_matrix<complex<double> >;
    extern void SetValue(MatrixImpl& in,unsigned int row,unsigned int col,complex<double> val);
    extern MatrixImpl MatrixExtend(const unsigned int dim,const MatrixImpl& a);
    extern MatrixImpl MatrixExtend(const MatrixImpl& a,const unsigned int dim);
    extern MatrixImpl operator*(const MatrixImpl& a,const MatrixImpl& b);
    extern MatrixImpl MatrixTensorMul(const MatrixImpl& a,const MatrixImpl& b);
    std::ostream& operator<<(std::ostream&,const MatrixImpl&);
    };



#endif /*__matrix_hpp_*/
