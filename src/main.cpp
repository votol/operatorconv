#include <iostream>
//#include <stdexcept>
//#include <string>
//#include <vector>
//#include "complex.h"
//#include "sparse.hpp"
//#include <thread> 
#include <chrono> 
#include <stdio.h>
#include <math.h>
#include <boost/numeric/ublas/io.hpp>
#include <stdlib.h>
#include <time.h>
#include "yaml-cpp/yaml.h"
#include "operators.hpp"

#define G parameter("G")

using namespace Abstr;




int main(int argc,char **argv)
    {
    ///real_function
    std::cout<<"Testing real function...\n";
    auto Rres=2.0*(t-5.0)/(6.0*pow(t,sqrt(3.2))+t);
    auto Rres1=t;
    for(unsigned int l=0;l<10;++l)
        {
        Rres1*=t;
        Rres1+=5.0;
        }
    std::cout<<Rres.to_string()<<std::endl;
    std::cout<<Rres1.to_string()<<std::endl;
    
    auto Rprov=sin(t+0.5);
    auto Rres3=sin(Rprov);
    Rprov=5.0;
    std::cout<<Rres3.to_string()<<std::endl;

    ///complex_function
    std::cout<<"Testing complex function...\n";
    auto Cres1=t*i;
    for(unsigned int l=0;l<10;++l)
        {
        Cres1*=t/i;
        Cres1+=5.0;
        }
    std::cout<<Cres1.to_string()<<std::endl;
    auto Cres2=(t+sin(t*i))/(t-cos(t*i));
    std::cout<<Cres2.to_string()<<std::endl;


    //yaml-cpp
    YAML::Node config = YAML::LoadFile(argv[1]);
    ///const operator

    std::cout<<"Testing constant operator...\n";


    Geometry::SetGeometry(config["geometry"]);
    std::vector<unsigned int> tmp_vec(400);
    srand(time(NULL));
    for(unsigned int peri1=0;peri1<tmp_vec.size();peri1++)
        tmp_vec[peri1]=int(double(Geometry::GetInstance().GetNumberOfParticles())*double(rand())/double(RAND_MAX));
    auto COtmp1=-0.5*i*P(tmp_vec[0]).a()*P(tmp_vec[1]).ak();
    for(unsigned int peri1=1;peri1<tmp_vec.size()/2;peri1++)
        COtmp1+=P(tmp_vec[peri1*2]).a()*P(tmp_vec[peri1*2+1]).ak()+0.3;

    //COtmp1.print();
    //auto COres1=COtmp1.calculate();
    //std::cout<<COres1<<std::endl;

    //operator
    std::cout<<"Testing operator...\n";
    Operator Otmp1;
    Otmp1=-cos(0.5*i*t)*conj(P(tmp_vec[0]).a()*P(tmp_vec[1]).ak());
    //Otmp1=-cos(0.5*i*t)*conj(P(2).a()*P(0).ak());
    //for(unsigned int peri1=0;peri1<tmp_vec.size();peri1++)
    //    std::cout<<tmp_vec[peri1]<<" ";
    //std::cout<<std::endl;
    for(unsigned int peri1=1;peri1<tmp_vec.size()/2;peri1++)
        Otmp1+=G*P(tmp_vec[peri1*2]).a()*P(tmp_vec[peri1*2+1]).ak()+0.3;

    Otmp1.save(config,"test operator");

    return 0;
    }
