#!/usr/bin/env python

import uuid
import os
import shutil
import argparse
import yaml
import colorama

#geting config file
parser = argparse.ArgumentParser(description='Getting yaml config ')
parser.add_argument('config_file', nargs=1, type=argparse.FileType('r'))
parser.add_argument('--verbose', '-v', dest='verbose', action='store_true')
args = parser.parse_args()

config_file_path = os.path.abspath(args.config_file[0].name)

yaml_stream = open(config_file_path, 'r')
config = yaml.load(yaml_stream)
yaml_stream.close()

#creating necessary folders in /tmp/OperatorConv
OpertorConv_directory = os.path.expanduser('~')+'/.operator-conv'
current_dir = os.getcwd()

work_directory = config['paths']['output_directory']+str(uuid.uuid4())
#work_directory='/tmp/OperatorConv/'+str(uuid.uuid4())
#if not os.path.isdir('/tmp/OperatorConv/'):
#    os.mkdir('/tmp/OperatorConv/')
os.mkdir(work_directory)
os.chdir(work_directory)
shutil.copytree(OpertorConv_directory+'/code/Libraries', work_directory+'/Libraries')
shutil.copytree(OpertorConv_directory+'/code/types', work_directory+'/types')
shutil.copy(OpertorConv_directory+'/code/CMakeLists.txt', work_directory)


#generating main.cpp
print(colorama.Fore.RED + 'Generating of the main.cpp...' + colorama.Style.RESET_ALL)
main = open('main.cpp', 'w')
main.write('#include "yaml-cpp/yaml.h"\n#include "operators.hpp"\n')
main.write('#include "general.h"\n')
for i in config['parameters']:
    if config['parameters'][i] is None:
        main.write('#define ' + i + ' parameter("' + i + '")\n')
    elif config['parameters'][i].get('substituent_type') is None:
        if config['parameters'][i].get('value') is None:
            main.write('#define '+i+' parameter("'+i+'")\n')
        else:
            main.write('#define ' + i + ' '+str(float(config['parameters'][i]['value']))+'\n')
    else:
        if config['parameters'][i]['substituent_type'] == 'value':
            main.write('#define ' + i + ' ' + str(float(config['parameters'][i]['value']))+'\n')
        elif config['parameters'][i]['substituent_type'] == 'name':
            main.write('#define ' + i + ' parameter("' + i + '")\n')
        else:
            raise RuntimeError('Wrong value for substituent_typr')
    #main.write('#define '+i+' '+repr(config['parametrs'][i])+'\n')
main.write('using namespace Abstr;\n')
main.write('int main(int argc,char **argv)\n    {\n')
main.write('    YAML::Node config = YAML::LoadFile(argv[1]);\n')
main.write('    Geometry::SetGeometry(config["geometry"]);\n')
id=0
for i in config['operators']:
    main.write('    Operator Oper'+str(id)+';\n')
    main.write('    Oper' + str(id) + '=' + str(i['operator']) + ';\n')
    id=id+1

main.write('    unsigned int number_of_matrices = 0;\n')

id = 0
for i in config['operators']:
    main.write('    number_of_matrices += Oper' + str(id) + '.GetNumberOfMatrices();\n')
    id += 1

main.write('    WriteNumberOfMatricesToFile(config,number_of_matrices);\n')

id = 0
for i in config['operators']:
    main.write('    Oper' + str(id) + '.save(config,"' + i['name'] + '");\n')
    id += 1
main.write('    }\n')
main.close()
#building
print(colorama.Fore.RED + 'Building auxiliary project...' + colorama.Style.RESET_ALL)
os.mkdir('build')
os.chdir('./build')
if args.verbose:
    os.system("cmake -D CMAKE_BUILD_TYPE=release ../")
    os.system("make -j `nproc` VERBOSE=")
else:
    os.system("cmake -D CMAKE_BUILD_TYPE=release ../ 1> /dev/null")
    os.system("make -j `nproc` VERBOSE= 1> /dev/null")
print(colorama.Fore.RED + 'Running auxiliary project...' + colorama.Style.RESET_ALL)
if args.verbose:
    print(colorama.Fore.RED + "./OperatorConvExm " + config_file_path + colorama.Style.RESET_ALL)
    os.system("./OperatorConvExm " + config_file_path)
else:
    os.system("./OperatorConvExm " + config_file_path + " 1> /dev/null")

print(colorama.Fore.RED + 'Finished' + colorama.Style.RESET_ALL)
os.chdir(current_dir)
shutil.rmtree(work_directory)

