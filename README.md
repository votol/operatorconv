# README #

### Header of the .bin ###
First 4 bytes - number of matrices saved in the file
### Structure of sparse matrix saved in .bin ###
First 4 bytes - number of nonzero elements (non_zero)

Next 4\*non_zero bytes - row index of nonzero elements

Next 4\*non_zero bytes - column index of nonzero elements

Next 16\*non_zero bytes - data in complex format: first 8 bytes - real part, second 8 bytes - imaginary part
